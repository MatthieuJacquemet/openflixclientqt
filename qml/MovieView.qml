import QtQuick 2.3
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4



Item {
    id: view
    property int media_id


    function close() {

        app.exitContext()
        // content_provider.exitContext()
        // content_handler.exitContext()

        pop()
    }

    StackView.onActivated: {
        if (media_id !== 0)
            app.getDetails(media_id)
    }

    Connections {
        target: app

        onSetDetails: {
            backdrop_img.source = backdrop
            title_logo.text = title
            synopsis_text.text = synopsis

            app.content(title, original_title, date)
        }

        onSetClearLogo: {
            if (url == "") {
                title_logo.visible = true
                clearlogo_container.opacity = 1
            }
            else {
                clear_logo.visible = true
                console.log(url)
                clear_logo.source = url
            }
        }

        onContentsChanged: {
            if (combo.content_list.rowCount() == 1) {
                combo.incrementCurrentIndex()
                play_button.enabled = true
            }
        }
    }

    // Shortcut {
    //     sequence: "escape"
    //     autoRepeat: false

    //     onActivated: view.close()
    // }

    Flickable {
        id: details_view
        anchors.fill: parent
        contentHeight: content.height

        Column {
            id: content
            width: parent.width
            
            Item {
                id: backdrop_container
                width: parent.width
                height: width * 0.5625
                clip: true

                Image {
                    id: backdrop_img
                    width: parent.width
                    height: (sourceSize.height / sourceSize.width) * width
                    opacity: 0
                    // fillMode: Image.PreserveAspectFit

                    onStatusChanged: {
                        if (status == Image.Ready)
                            opacity = 1
                    }

                    Behavior on opacity { 
                        SmoothedAnimation { 
                            velocity: 2
                        }
                    }
                }
            }

            Item {
                id: details_container
                width: parent.width
                height: 1000

                Rectangle {
                    width: parent.width
                    height: 400
                    anchors.bottom: parent.top

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "transparent" }
                        GradientStop { position: 1.0; color: "black" }
                    }
                }


                Text {
                    id : synopsis_text
                    anchors.bottom: clearlogo_container.top
                    width: 700
                    height: 500
                    color: "white"
                    wrapMode: Text.WordWrap 
                    font.pointSize: 12
                }

                Item {
                    id: clearlogo_container
                    anchors.bottom: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 40
                    width: 500
                    opacity: 0

                    Behavior on opacity { 
                        SmoothedAnimation { 
                            velocity: 2
                        }
                    }

                    Image {
                        id: clear_logo
                        fillMode: Image.PreserveAspectFit
                        width: parent.width
                        anchors.bottom: parent.bottom
                        visible: false

                        onStatusChanged: {
                            if (status == Image.Ready)
                                clearlogo_container.opacity = 1
                        }
                    }

                    Text {
                        id: title_logo
                        font.pointSize: 50
                        font.weight: Font.Bold
                        wrapMode: Text.WordWrap 
                        width: parent.width
                        color: "white"
                        visible: false
                        anchors.bottom: parent.bottom
                    }
                }

                Row {
                    anchors.right: parent.right
                    anchors.bottom: parent.top
                    anchors.rightMargin: 20
                    anchors.bottomMargin: 20
                    height: 30
                    spacing: 20

                    Button {
                        id: play_button
                        height: parent.height
                        enabled: false
                        // display: AbstractButton.TextOnly
                        // text: "play"

                        background: Rectangle {
                            radius: 5
                            color: "green"
                        }
                        Image {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter

                            height: parent.height*0.7
                            source: "/images/play-button.svg"
                            fillMode: Image.PreserveAspectFit
                            sourceSize.width: parent.width
                            sourceSize.height: parent.height
                        }
                        onClicked: {
                            let data = combo.content_list.get(combo.currentIndex)
                            push(player, {"content_data": data, "title": title_logo.text})
                        }
                    }

                    ComboBox {
                        id: combo
                        width: 200
                        height: parent.height

                        function get_size(size) {

                            var num = Math.floor(Math.log(size)/Math.log(1024));
                            let units = ["B", "kB", "MB", "GB", "TB"]
                            let unit = " " + units[num]
                            return (size/Math.pow(1024, num)).toFixed(2) + unit;
                        }

                        onActivated: {
                            console.log("activated "  + currentIndex)
                            let data = content_list.get(currentIndex)
                            
                            let props = {"name": data["name"],
                                         "size": get_size(data["size"])}

                            contentItem = content_row.createObject(combo, props)
                        }

                        model: app.contentModel
                        property var content_list: model
                        // model: ListModel {
                        //     id: content_list
                        //     onCountChanged: {
                        //         if (count == 1)
                        //             combo.incrementCurrentIndex()
                        //     }
                        // }

                        Component {
                            id: content_row
                            Row {
                                id: container
                                property string name
                                property string size

                                Text {
                                    id: combo_label
                                    verticalAlignment: Text.AlignVCenter
                                    font.pointSize: 12
                                    color: "black"
                                    text: container.name
                                    width: 120
                                    height: parent.height
                                    elide: Text.ElideRight
                                }
                                Text {
                                    text: container.size
                                    font.pointSize: 12
                                    verticalAlignment: Text.AlignVCenter
                                    height: parent.height
                                }
                            }
                        }

                        Component {
                            id: player

                            VideoPlayer {}
                        }

                        background: Rectangle {
                            id: clipper
                            clip: true
                            color: "transparent"

                            Rectangle {
                                id: combo_background
                                signal opened()
                                signal closed()
                                
                                radius: 5
                                color: "green"
                                height: parent.height
                                width: parent.width

                                onClosed: {
                                    if (combo.model.count > 0) {
                                        height = parent.height
                                        y = parent.y
                                    }
                                }
                                onOpened: {
                                    if (combo.model.count > 0) {
                                        height = parent.height + radius
                                        if (combo.popup.y < y)
                                            y = -radius
                                    }
                                }
                            }
                        }

                        popup.onOpened: combo_background.opened()
                        popup.onClosed: combo_background.closed()

                        delegate: ItemDelegate {
                            id: item_delegate
                            width: combo.width
                            highlighted: ListView.isCurrentItem
                            contentItem: Loader { 
                                sourceComponent: content_row
                                onLoaded: {
                                    item.name = name
                                    item.size = combo.get_size(size)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Component {
        id: content_entry

        Text {
            text: content_name
            MouseArea {
                anchors.fill: parent
                onClicked: combo.popup.close()
            }
        }
    }

    // Connections {
    //     target: app
    //     onAppendContent: {
    //         content_list.append({"content_name": name,
    //                             "content_provider": provider,
    //                             "content_size": size,
    //                             "content_type": type,
    //                             "content_data": data})
    //     }
    // }

    Button {
        id: back_button
        anchors.top: view.top
        anchors.left: view.left
        anchors.leftMargin: 20
        anchors.topMargin: anchors.leftMargin
        width: 30
        height: width

        // icon.sourceSize.width: back_button.width
        // icon.sourceSize.height: back_button.height
        // icon.color: "transparent"
        Image {
            source: "/images/back_arrow.svg"
            sourceSize.width: parent.width
            sourceSize.height: parent.height
        }
        background: null

        onClicked: view.close()
    }

    DropShadow {
        anchors.fill: back_button
        verticalOffset: 1
        radius: 2
        samples: 5
        color: "black"
        source: back_button
        cached: true
    }
}







