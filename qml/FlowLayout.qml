import QtQuick 2.9
import QtQuick.Controls 2.0

Item {
    id: layout

    property bool ready: false
    property int hspacing: 20
    property int vspacing: 20
    property int bottom_limit: 0

    property Component delegate
    property var model

    signal nearBottom()

    Timer {
        id: timer
        interval: 500
        repeat: true
        triggeredOnStart: true
        onTriggered: nearBottom()
    }

    Flickable {
        id: scroll_area
        anchors.fill: parent
        property bool _beyond_limit: false

        ScrollBar.vertical: ScrollBar {}


        onWidthChanged:     _repeater.recomputeAllChildren()
        onHeightChanged:    _repeater.updateItemsVisibility()
        onContentYChanged:  _repeater.updateItemsVisibility() , checkBottom()
        onContentHeightChanged: checkBottom()

        function checkBottom() {
            
            let viewport_bottom = contentY + height
            let event_limit = contentHeight - layout.bottom_limit

            if (viewport_bottom >= event_limit) {
                // if (!timer.running)
                //     nearBottom()
                timer.start()
            }
            else
                timer.stop()


            // if (viewport_bottom >= event_limit) {
            //     if (!_beyond_limit) {
            //         _beyond_limit = true
            //         nearBottom()}
            // } else
            //     _beyond_limit = false
        }

        Repeater {
            id: _repeater
            delegate: layout.delegate
            model: layout.model

            property int _item_width: 0
            property int _item_height: 0

            property int _before_start_index: 0
            property int _before_end_index: 0
            property int _spacing: 0
            property int _col: 0
            property int _row: 0
            property int _x: 0
            property int _y: 0


            onItemAdded: {
                
                if (_item_width == 0 && _item_height == 0) {
                    _item_width = item.width
                    _item_height = item.height
                    recomputeSpacing()
                    updateItemsVisibility()
                }
                
                recomputeChildPosition(index)

                scroll_area.contentHeight = _y + getRowHeight()

                if (index < _before_start_index || index > _before_end_index)
                    item.visible = false
                
                // timer.restart()
            }

            function reset() {

                scroll_area.contentHeight = 0
                _before_start_index = 0
                _before_end_index = getVisibleRows() * _col
                _x = _spacing
                _y = hspacing
            }

            function getRowHeight() {

                return _item_height + layout.hspacing
            }

            function getVisibleRows() {

                return Math.ceil(scroll_area.height / getRowHeight()) + 1
            }

            function updateItemsVisibility() {
                
                let pos = scroll_area.contentY

                if (count == 0 || pos < 0)
                    return
                
                let start_row = Math.floor(pos / getRowHeight())
                let start_index = start_row * _col

                let rows = getVisibleRows()
                let end_index = start_index + rows*_col


                if (start_index != _before_start_index || 
                    end_index != _before_end_index) 
                {
                    let end = Math.min(count, _before_end_index)

                    for (var i = _before_start_index; i<end; i++)
                        itemAt(i).visible = false
                    
                    end = Math.min(count, end_index)

                    for (var i = start_index; i<end; i++)
                        itemAt(i).visible = true
                    
                    _before_start_index = start_index
                    _before_end_index = end_index
                }
            }

            function recomputeChildPosition(index) {
                
                let child = itemAt(index)

                if ((index % _col) == 0 && index != 0) {
                    _x = _spacing
                    _y += getRowHeight()
                }
                
                child.x = _x
                child.y = _y

                _x += _spacing + _item_width
            }

            function recomputeSpacing() {
                
                let hspacing = layout.hspacing
                let item_width = _item_width + vspacing

                _col = Math.floor((scroll_area.width - hspacing) / item_width)
                _row = Math.ceil(count / _col)
                
                let available = scroll_area.width - _col*_item_width
                _spacing = available / (_col + 1)

                _x = _spacing
                _y = hspacing
            }

            function recomputeAllChildren() {
                
                if (count == 0)
                    return

                recomputeSpacing()

                for (var i=0; i<count; i++)
                    recomputeChildPosition(i)

                scroll_area.contentHeight = _y + getRowHeight()
                
                updateItemsVisibility(true)
            }
        }
    }
    
    function append(item) {
        list_model.append(item)
    }

    function clear() {
        _repeater.model.clear()
        _repeater.reset()
    }
}

