import QtQuick 2.9
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4

import VLCQt 1.1

Item {
    id: view
    property string url

    function close() {
        app.stopContent()
        pop()
    }

    Rectangle {
        id: video_view
        color: "black"
        anchors.fill: parent
    
        VlcVideoOutput {
            id: video_output
            anchors.fill : parent

            source: VlcPlayer {
                id: video_player
                url: view.url //"file:///tmp/torrent-stream/7969848312256d373905239864d2439bdf09a661/La Reine Des Neiges II 2019 1080p VFF EN X264 AC3-mHDgz.mkv" 
                // logLevel: Vlc.DebugLevel
                
                function togglePlay() {
                    
                    if(state == Vlc.Playing)
                        pause()
                    else
                        play()
                }

                function seek_ms(ms) {

                    position += ms/length
                }

                onStateChanged: {
                    if(state == Vlc.Playing)
                        play_pause_img.source = "/images/pause.svg"
                    else
                        play_pause_img.source = "/images/play-button.svg"
                }

                onPositionChanged: {
                    if (!seekbar.pressed)
                        seekbar.value = position
                }
            }
        }
    }

    Item {
        id: overlay
        anchors.fill: parent


        NumberAnimation on opacity {
            id: fading_animation
            from: 1
            to: 0
            duration: 1000

            onRunningChanged: {
                if (!running) {
                    control_panel.visible = false
                    view_mouse_area.cursorShape = Qt.BlankCursor
                }
            }
        }

        MouseArea {
            id: view_mouse_area
            anchors.fill: parent
            hoverEnabled: true
            // onDoubleClicked: window.toggle_fullscreen()
            onClicked: {
                if (play_timer.running) {
                    play_timer.stop()
                    window.toggle_fullscreen()
                }
                else {
                    play_timer.restart()
                }
            }

            Timer {
                id: play_timer
                interval: 200
                onTriggered: video_player.togglePlay()
            }

            Timer {
                id: overlay_timer
                interval: 4000

                onTriggered: fading_animation.start()
            }

            onPositionChanged: {
                fading_animation.stop()
                overlay_timer.restart()
                overlay.opacity = 1
                control_panel.visible = true
                view_mouse_area.cursorShape = Qt.ArrowCursor
            }
        }

        Item {
            id: control_panel
            width: 600
            height: 100
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 75
            anchors.horizontalCenter: parent.horizontalCenter
    
            layer.enabled: true
            layer.effect: OpacityMask {
                maskSource: Item {
                    width: control_panel.width
                    height: control_panel.height

                    Rectangle {
                        anchors.fill: parent
                        radius: 10
                    }
                }
            }

            ShaderEffectSource {
                id: background_source

                sourceItem: video_view
                x: control_panel.x
                y: control_panel.y
                width: control_panel.width
                height: control_panel.height
                sourceRect: Qt.rect(x,y, width, height)
                visible: false
            }
        

            FastBlur {
                id: blur
                anchors.fill: control_panel
                source: background_source
                radius: 128
                visible: false
            }

            // FastBlur {
            //     id: blur2
            //     anchors.fill: control_panel
            //     source: blur
            //     radius: 32
            // }

            Rectangle {
                id: white_rect
                anchors.fill: parent
                color: "#323232"
                visible: false
            }

            Blend {
                id: blend
                anchors.fill: parent
                source: blur
                foregroundSource: white_rect
                mode: "screen"

            }

            // ColorOverlay {
            //     id: color_overlay
            //     anchors.fill: parent
            //     source: blur
            //     color: "#1e1e1e"
            //     opacity: 0.2
            // }

            Slider {
                id: seekbar
                anchors.top: parent.top
                anchors.topMargin: 10
                anchors.horizontalCenter: parent.horizontalCenter
                value: 0
                
                handle.height: 15
                handle.width: handle.height

                background: Rectangle {
                    x: seekbar.leftPadding
                    y: seekbar.topPadding + (seekbar.availableHeight - height) / 2
                    implicitWidth: 500
                    implicitHeight: 4
                    width: seekbar.availableWidth
                    height: implicitHeight
                    radius: 2
                    color: "#40ffffff"

                    Rectangle {
                        width: seekbar.visualPosition * parent.width
                        height: parent.height
                        color: "white"
                        radius: 2
                    }
                }


                onValueChanged: {
                    if (pressed)
                        video_player.position = value
                }
            }
            Row {
                anchors.top: seekbar.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                // anchors.topMargin: 10
                spacing: 20

                Button {
                    id: seek_back_button
                    width: 30
                    height: width

                    background: null

                    onClicked: video_player.seek_ms(-5000)

                    Image {
                        fillMode: Image.PreserveAspectFit
                        height: parent.height
                        source: "/images/seek-arrow.svg"
                        mirror: true
                    }
                }

                Button {
                    id: play_pause_button
                    width: 30
                    height: width

                    background: null

                    Image {
                        id: play_pause_img
                        source: "/images/pause.svg"
                        fillMode: Image.PreserveAspectFit
                        height: parent.height
                    }

                    onClicked: video_player.togglePlay()
                }

                Button {
                    id: seek_forward_button
                    width: 30
                    height: width

                    background: null

                    onClicked: video_player.seek_ms(5000)

                    Image {
                        fillMode: Image.PreserveAspectFit
                        height: parent.height
                        source: "/images/seek-arrow.svg"
                    }
                }
            }
        }

        Button {
            id: back_button
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.topMargin: anchors.leftMargin
            width: 50
            height: width
            icon.source: "/images/back_arrow.svg"
            icon.color: "transparent"

            background: null

            onClicked: view.close()
        }
    }

}