import QtQuick 2.9
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4


ApplicationWindow {

    id: window
    visible: true
    width: 1280
    height: 720
    title: "OpenFlix"
    color: "black"

    property int prev_x: x
    property int prev_y: y
    property int prev_w: width
    property int prev_h: height

    // property int fps_counter: 0
    // onFrameSwapped: fps_counter++

    // Timer {
    //     running: true
    //     repeat: true
    //     interval: 1000
    //     onTriggered: {
    //         fps.text = "fps : " + window.fps_counter
    //         window.fps_counter = 0
    //     }
    // }

    // Text {
    //     id: fps
    //     color: "red"
    //     z: 10
    // }

    function backup_rect() {

        prev_x = x
        prev_y = y
        prev_w = width
        prev_h = height
    }

    function restore_rect() {

        x = prev_x
        y = prev_y
        width = prev_w
        height = prev_h
    }

    function toggle_fullscreen() {

        if (window.visibility == ApplicationWindow.FullScreen) {
            window.showNormal()
            restore_rect()
        }
        else {
            backup_rect()
            window.showFullScreen()
        }
    }

    Shortcut {
        sequence: "f11"
        autoRepeat: false

        onActivated: toggle_fullscreen()
    }

    Component {
        id: main_view
        
        MainView {}
    }
    
    StackView {
        id: view_stack
        anchors.fill: parent
        initialItem: main_view
    }
}







