import QtQuick 2.9
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

// import QtAV 1.7

Item {
    id: view
    property string title
    property variant content_data

    // function close() {
    //     player.stop()
    //     player.source = ""
    //     player.destroy()
    //     app.stop_content()
    //     pop()
    // }

    // function get_timestamp(time_ns) {
        
    //     let secs = Math.floor(time_ns / 1000)
    //     let mins = Math.floor(secs / 60)
    //     let hours = Math.floor(mins / 60)

    //     let items = [String(mins % 60).padStart(2, '0'),
    //                  String(secs % 60).padStart(2, '0')]

    //     console.log(secs)
    
    //     if (hours > 0)
    //         items.unshift(hours)

    //     return items.join(":")
    // }

    // Component.onCompleted: {
    //     app.start_content(  view.content_data["type"],
    //                         view.content_data["data"])
    // }

    // Connections {
    //     target: app

    //     function onContentReady(uri) {
    //         player.source = uri
    //         player.play()
    //     }
    // }


    // MediaPlayer {
    //     id: player
    //     bufferMode: MediaPlayer.BufferTime
    //     timeout: 120000

    //     // videoCodecPriority: {
    //     //     switch (Qt.platform.os) {
    //     //         case "linux": return ["VAAPI", "FFmpeg"]
    //     //         case "osx": return ["VideoToolbox", "FFmpeg"]
    //     //         default: return ["DXVA", "D3D11", "FFmpeg"]
    //     //     }
    //     // }

    //     // videoCodecOptions: {
    //     //     var options = { 
    //     //         display: "GLX",
    //     //         copyMode: "ZeroCopy"
    //     //     }

    //     //     return options;
    //     // }

    //     function togglePlay() {
            
    //         if(playbackState == MediaPlayer.PlayingState)
    //             pause()
    //         else
    //             play()
    //     }

    //     function seek_ms(ms) {
    //         seek(position + ms)
    //     }

    //     onPlaybackStateChanged: {

    //         if (playbackState == MediaPlayer.PlayingState) {
    //             total_time.text = view.get_timestamp(duration)
    //             seekbar.to = duration
    //             play_pause_img.source = "/images/pause.svg"
    //         } else
    //             play_pause_img.source = "/images/play-button.svg"
    //     }

    //     onPositionChanged: {
    //         if (!seekbar.pressed)
    //             seekbar.value = position
            
    //         elapsed_timestamp.text = view.get_timestamp(player.position)
    //     }

    //     onStatusChanged:
    //         busy_indicator.running = (status == MediaPlayer.Buffering)
    // }

    // Rectangle {
    //     id: video_view
    //     color: "black"
    //     anchors.fill: parent

    //     VideoOutput2 {
    //         id: video_output
    //         opengl: true
    //         fillMode: VideoOutput.PreserveAspectFit
    //         anchors.fill: parent
    //         source: player
    //     }

    //     BusyIndicator {
    //         id: busy_indicator
    //         z: 8

    //         anchors.horizontalCenter: parent.horizontalCenter
    //         anchors.verticalCenter: parent.verticalCenter

    //         width: 120
    //         height: 120

    //         running: true

    //         contentItem: Item {
    //             anchors.fill: parent
    //             opacity: busy_indicator.running
                
    //             Behavior on opacity {
    //                 OpacityAnimator {
    //                     duration: 250
    //                 }
    //             }

    //             Image {
    //                 id: spin_indicator
    //                 anchors.fill: parent
    //                 source: "/images/loader-spin.svg"
    //                 smooth: true

    //                 RotationAnimator on rotation {
    //                     loops: Animation.Infinite
    //                     duration: 2000
    //                     from: 0 ; to: 360
    //                 }

    //                 // RotationAnimator {
    //                 //     target: item
    //                 //     from: 0
    //                 //     to: 360
    //                 //     loops: Animation.Infinite
    //                 //     duration: 1250
    //                 // }
    //             }
                
    //         }
    //     }
    //     DropShadow {
    //         anchors.fill: busy_indicator
    //         verticalOffset: 1
    //         radius: 2
    //         samples: 5
    //         color: "black"
    //         source: busy_indicator
    //         cached: false
    //     }
    // }

    // Item {
    //     id: overlay
    //     anchors.fill: parent


    //     NumberAnimation on opacity {
    //         id: fading_animation
    //         from: 1
    //         to: 0
    //         duration: 1000

    //         onRunningChanged: {
    //             if (!running) {
    //                 // control_panel.visible = false
    //                 view_mouse_area.cursorShape = Qt.BlankCursor
    //             }
    //         }
    //     }

    //     MouseArea {
    //         id: view_mouse_area
    //         anchors.fill: parent
    //         hoverEnabled: true
    //         // onDoubleClicked: window.toggle_fullscreen()
    //         onClicked: {
    //             if (play_timer.running) {
    //                 play_timer.stop()
    //                 window.toggle_fullscreen()
    //             }
    //             else {
    //                 play_timer.restart()
    //             }
    //         }

    //         Timer {
    //             id: play_timer
    //             interval: 200
    //             onTriggered: player.togglePlay()
    //         }

    //         Timer {
    //             id: overlay_timer
    //             interval: 4000

    //             onTriggered: fading_animation.start()
    //         }

    //         onPositionChanged: {
    //             fading_animation.stop()
    //             overlay_timer.restart()
    //             overlay.opacity = 1
    //             // control_panel.visible = true
    //             view_mouse_area.cursorShape = Qt.ArrowCursor
    //         }
    //     }

    //     Rectangle {
    //         width: parent.width
    //         height: 300
    //         anchors.bottom: parent.bottom

    //         gradient: Gradient {
    //             GradientStop { position: 0.0; color: "transparent" }
    //             GradientStop { position: 1.0; color: "black" }
    //         }

    //         RowLayout {
    //             id: seekbar_row
    //             spacing: 10
    //             anchors.left: parent.left
    //             anchors.right: parent.right
    //             anchors.leftMargin: 10
    //             anchors.rightMargin: 10
    //             anchors.bottom: controls_row.top
    //             anchors.bottomMargin: 20

    //             Text {
    //                 id: elapsed_timestamp
    //                 text: "--:--"
    //                 color: "white"
    //                 font.family: "Open Sans"
    //                 font.pointSize: 11
    //                 horizontalAlignment: Text.AlignRight
    //                 Layout.preferredWidth: 60
    //                 Layout.alignment: Qt.AlignCenter
    //             }

    //             Slider {
    //                 id: seekbar
    //                 Layout.fillWidth: true
    //                 Layout.preferredHeight: 8
    //                 Layout.alignment: Qt.AlignCenter

    //                 value: 0
    //                 handle: null

    //                 background: Rectangle {
    //                     width: seekbar.availableWidth
    //                     height: 4
    //                     radius: 2
    //                     color: "#20ffffff"

    //                     Rectangle {
    //                         width: seekbar.visualPosition * parent.width
    //                         height: parent.height
    //                         color: "#005bd1"
    //                         radius: parent.radius
    //                     }
    //                 }

    //                 onValueChanged: {
    //                     if (pressed)
    //                         player.seek(value)
    //                 }
    //             }

    //             Text {
    //                 id: total_time
    //                 text: "--:--"
    //                 color: "white"
    //                 font.family: "Open Sans"
    //                 font.pointSize: 11
    //                 horizontalAlignment: Text.AlignLeft
    //                 Layout.preferredWidth: 60
    //                 Layout.alignment: Qt.AlignCenter
    //             }
    //         }

    //         RowLayout {
    //             id: controls_row
    //             anchors.bottom: parent.bottom
    //             anchors.bottomMargin: 20
    //             anchors.horizontalCenter: parent.horizontalCenter
    //             spacing: 20

    //             Button {
    //                 id: seek_back_button
    //                 width: 30
    //                 height: width

    //                 background: null

    //                 onClicked: player.seek_ms(-5000)
    //                 // icon.source: "images/seek-arrow.svg"
    //                 // icon.width: width
    //                 // icon.height: height
    //                 // display: AbstractButton.IconOnly
    //                 Image {
    //                     id: seek_back_icon
    //                     fillMode: Image.PreserveAspectFit
    //                     // height: parent.height
    //                     source: "/images/seek-arrow.svg"
    //                     sourceSize.width: parent.width
    //                     sourceSize.height: parent.height
    //                     mirror: true
    //                 }
    //             }

    //             Button {
    //                 id: play_pause_button
    //                 width: 30
    //                 height: width

    //                 background: null

    //                 Image {
    //                     id: play_pause_img
    //                     source: "/images/pause.svg"
    //                     fillMode: Image.PreserveAspectFit
    //                     // height: parent.height
    //                     sourceSize.width: parent.width
    //                     sourceSize.height: parent.height
    //                 }

    //                 onClicked: player.togglePlay()
    //             }

    //             Button {
    //                 id: seek_forward_button
    //                 width: 30
    //                 height: width

    //                 background: null

    //                 onClicked: player.seek_ms(5000)

    //                 Image {
    //                     id: seek_backward_icon
    //                     fillMode: Image.PreserveAspectFit
    //                     // height: parent.height
    //                     source: "/images/seek-arrow.svg"
    //                     sourceSize.width: parent.width
    //                     sourceSize.height: parent.height
    //                 }
    //             }
    //         }
    //     }

    //     Rectangle {
    //         width: parent.width
    //         height: 200
    //         anchors.top: parent.top

    //         gradient: Gradient {
    //             GradientStop { position: 1.0; color: "transparent" }
    //             GradientStop { position: 0.0; color: "black" }
    //         }

    //         Button {
    //             id: back_button
    //             anchors.top: parent.top
    //             anchors.left: parent.left
    //             anchors.leftMargin: 20
    //             anchors.topMargin: anchors.leftMargin
    //             width: 40
    //             height: width
    //             // icon.source: "/images/back_arrow.svg"
    //             // icon.color: "transparent"


    //             Image {
    //                 fillMode: Image.PreserveAspectFit
    //                 // height: parent.height
    //                 source: "/images/back_arrow.svg"
    //                 sourceSize.width: parent.width
    //                 sourceSize.height: parent.height
    //             }

    //             background: null

    //             onClicked: view.close()
    //         }

    //         Text {
    //             color: "white"
    //             text: view.title
    //             font.pointSize: 25
    //             anchors.left: back_button.right
    //             anchors.top: parent.top
    //             anchors.topMargin: 20
    //             anchors.leftMargin: 50
    //         }
    //     }
    // }
}






























//         Item {
//             id: control_panel
//             width: 600
//             height: 100
//             anchors.bottom: parent.bottom
//             anchors.bottomMargin: 75
//             anchors.horizontalCenter: parent.horizontalCenter


// ////////////////////////////////////////////
//             layer.enabled: true
//             layer.effect: OpacityMask {
//                 maskSource: Item {
//                     width: control_panel.width
//                     height: control_panel.height

//                     Rectangle {
//                         anchors.fill: parent
//                         radius: 10
//                     }
//                 }
//             }

//             ShaderEffectSource {
//                 id: background_source

//                 sourceItem: video_view
//                 x: control_panel.x
//                 y: control_panel.y
//                 width: control_panel.width
//                 height: control_panel.height
//                 sourceRect: Qt.rect(x,y, width, height)
//                 visible: false
//             }
        

//             FastBlur {
//                 id: blur
//                 anchors.fill: control_panel
//                 source: background_source
//                 radius: 128
//                 visible: false
//             }
// ////////////////////////////////////








//             // FastBlur {
//             //     id: blur2
//             //     anchors.fill: control_panel
//             //     source: blur
//             //     radius: 32
//             // }



// ////////////////////////////////////
//             Rectangle {
//                 id: white_rect
//                 anchors.fill: parent
//                 color: "#323232"
//                 visible: false
//             }

//             Blend {
//                 id: blend
//                 anchors.fill: parent
//                 source: blur
//                 foregroundSource: white_rect
//                 mode: "screen"

//             }
// ////////////////////////////////////////





//             // ColorOverlay {
//             //     id: color_overlay
//             //     anchors.fill: parent
//             //     source: blur
//             //     color: "#1e1e1e"
//             //     opacity: 0.2
//             // }
