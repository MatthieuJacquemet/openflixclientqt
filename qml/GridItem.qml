import QtQuick 2.9
import QtGraphicalEffects 1.0


Item {

    width: 150;
    height: 280;
    id: item;
    // visible: false

    property string item_poster: "" //: "../poster.jpg"
    property string item_title: ""
    property string item_year: ""
    property int item_id: 0

    signal enter()

    Item {

        id: container
        width: parent.width
        height: 230
        opacity: 0
        scale: 0.5
        // layer.enabled: true

        Behavior on scale { 
            SmoothedAnimation { 
                velocity: 1.5
            }
        }
        Behavior on opacity { 
            SmoothedAnimation { 
                velocity: 1
            }
        }
        // transitions: [

        //     Transition {
        //         from: "*"
        //         to: "ready"
        //         NumberAnimation {
        //             target: container
        //             properties: "scale"
        //             from: 0.5
        //             to: 1
        //             duration: 250
        //             easing.type: Easing.OutQuad
        //         }
        //         NumberAnimation {
        //             target: container
        //             properties: "opacity"
        //             from: 0
        //             to: 1
        //             duration: 300
        //             easing.type: Easing.InQuad
        //         }
        //     }
        // ]

        // DropShadow {
        //     // anchors.fill: poster
        //     width: poster.width
        //     height: poster.height
        //     verticalOffset: 15
        //     radius: 10
        //     samples: 10
        //     color: "black"
        //     source: poster
        //     scale: poster.scale
        //     // cached: true
        // }


        Image {
            id: shadow
            source: "/images/shadow.png"
            width: poster.width
            height: poster.height
            scale: poster.scale*1.2
            y: 10
        }

        Image {
            id: poster
            width: parent.width
            source: item.item_poster
            height: 230;
            smooth: true

            onStatusChanged: {
                if (status == Image.Ready) {
                    console.log("image loaded ", source)
                    container.scale = 1
                    container.opacity = 1
                }
                else if (status == Image.Error) {
                    source = "/images/missing_poster.jpg"
                    container.scale = 1
                    container.opacity = 1
                }
            }

            MouseArea {
                id: poster_mouse_area
                hoverEnabled: true
                anchors.fill: parent

                onContainsMouseChanged: {
                    
                    if (containsMouse) {
                        // poster.scale = 1.2
                        poster.state = "mouseIn"
                        item.z = 1
                        if (title.overflow) {
                            title.elide = Text.ElideNone
                            text_scroll.start()
                        }
                    } else {
                        // poster.scale = 1
                        poster.state = "mouseOut"
                        title.elide = Text.ElideRight
                        text_scroll.stop()
                        title.x = 0
                        item.z = 0
                    }
                }

                onClicked: enter()
            }

            // Behavior on scale { 
            //     SmoothedAnimation { 
            //         velocity: 1
            //     }
            // }

            transitions: [

                Transition {
                    from: "*"
                    to: "mouseIn"
                    NumberAnimation {
                        target: poster
                        properties: "scale"
                        from: poster.scale
                        to: 1.2
                        duration: Math.abs(to - poster.scale) * 1000
                        easing.type: Easing.OutQuad
                    }
                },
                Transition {
                    from: "*"
                    to: "mouseOut"
                    NumberAnimation {
                        target: poster
                        properties: "scale"
                        from: poster.scale
                        to: 1
                        duration: Math.abs(to - poster.scale) * 1000
                        easing.type: Easing.OutQuad
                    }
                }
            ]

            Item {
            
                id: title_container
                anchors.top: poster.bottom
                width: parent.width
                height: 20
                clip: true
                
                Text {
                    id: title
                    text: item.item_title
                    font.pointSize: 10
                    width: parent.width
                    color: "white"

                    property bool overflow: false

                    SequentialAnimation {
                        id: text_scroll

                        NumberAnimation {
                            target: title
                            properties: "x"
                            from: 0
                            to: -title.contentWidth
                            duration: title.contentWidth * 20
                            easing.type: Easing.Linear
                        }
                        NumberAnimation {
                            target: title
                            properties: "x"
                            from: item.width
                            to: -title.contentWidth
                            duration: (title.contentWidth + item.width) * 20
                            easing.type: Easing.Linear
                            loops: Animation.Infinite
                        }
                    }
                    Component.onCompleted: {

                        if (contentWidth > title_container.width) {
                            overflow = true
                            elide = Text.ElideRight
                        }
                    }
                }
            }

            Text {
                id: year
                font.pointSize: 10
                text: item.item_year
                width: parent.width
                color: "gray"
                anchors.top: title_container.bottom
            }
        }
    }
}