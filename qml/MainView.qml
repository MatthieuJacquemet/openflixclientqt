import QtQuick 2.9
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4

Item {
    id: view
    Component.onCompleted: app.fetchMoreItems()

    // signal openMedia(int media_id)

    Component {
        id: movie_view

        MovieView {}
    }

    Component {
        id: tv_view

        MovieView {}
    }
    
    MouseArea {
        id: outside_text
        anchors.fill: parent
        onPressed: forceActiveFocus(), z = 0
    }

    Item {
        id: main_view
        anchors.fill: parent

        Image {
            source: "/images/background.jpg"
            anchors.fill: parent
        }

        FlowLayout {
        
            id: grid_view
            antialiasing: false
            width: parent.width
            bottom_limit: 2000
            objectName: "grid_view"
            height: parent.height - top_bar.height
            y: top_bar.height

             //  GridManager.initGridView()
            
            Text {
                id: no_results
                text: qsTr("No results found");
                font.pointSize: 20
                color: "white"
                visible: false
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }

            delegate: GridItem {
                item_title: title
                item_year: year
                item_poster: poster
                item_id: id

                // Component.onCompleted: console.log(title)

                onEnter: {
                    // openMedia(item_obj_id)
                    push( movie_view , {"media_id": item_id})
                }
            }

            model: app.itemModel

            Connections {
                target: app
                // onAppendGridItem: {
                //     grid_view.append({
                //             "item_title": title,
                //             "item_year": year,
                //             "item_poster_url": poster_url,
                //             "item_obj_id": id})
                // }
                function onNoResults() {
                    no_results.visible = true
                }
            }

            onNearBottom: {
                // console.log(count)
                app.fetchMoreItems()
            }
        }
    }

    Rectangle {
        id: top_bar
        width: parent.width
        height: 40
        color: "black"
        z: 1
        
        DropShadow {
            anchors.fill: top_bar
            verticalOffset: 0
            radius: 10
            samples: 10
            color: "black"
            source: top_bar
        }

        ShaderEffectSource {
            id: effect_source

            sourceItem: main_view
            anchors.fill: top_bar
            sourceRect: Qt.rect(x,y, width, height)
            visible: false
        }

        FastBlur {
            id: blur
            anchors.fill: top_bar
            source: effect_source
            radius: 96
        }

        ColorOverlay {
            id: top_bar_overlay
            anchors.fill: top_bar
            source: blur
            color: "black"
            opacity: 0.7
        }
    }

    TextField {
        id: search
        placeholderText: qsTr("search")
        anchors.right: top_bar.right
        anchors.verticalCenter: top_bar.verticalCenter
        anchors.rightMargin: 30
        implicitWidth: 200
        implicitHeight: 28
        color: "white"
        font.pointSize: 11
        selectByMouse: true
        z: 3

        Rectangle {
            id: white_rect
            color: "#303030"
            anchors.fill: parent
            visible: false
        }

        background: Rectangle {

            radius: height*0.5
            anchors.fill: parent
            clip: true
            color: "transparent"

            Blend {
                id: blend
                anchors.fill: parent
                source: top_bar_source
                foregroundSource: white_rect
                mode: "addition"

                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: Item {
                        width: blend.width
                        height: blend.height

                        Rectangle {
                            anchors.fill: parent
                            radius: height*0.5
                        }
                    }
                }
            }
        }

        ShaderEffectSource {
            id: top_bar_source

            sourceItem: top_bar
            x: search.x
            y: search.y
            width: search.width
            height: search.height
            sourceRect: Qt.rect(x,y, width, height)
            visible: false
        }
        
        onFocusChanged: {

            if (focus) {
                implicitWidth = 350
                outside_text.z = 2
            }
            else
                implicitWidth = 200
        }

        onTextChanged: {
            input_timer.restart()
        }

        Behavior on implicitWidth { 
            SmoothedAnimation { 
                velocity: 500
                easing.type: Easing.OutQuad
            }
        }

        Timer {
            id: input_timer
            interval: 200
            onTriggered: {
                app.search(search.text)
                grid_view.clear()
                no_results.visible = false
            }
        }
    }
}