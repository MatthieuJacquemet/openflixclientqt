# Openflix

![](https://gitlab.com/MatthieuJacquemet/storage/-/raw/master/files/openflix.jpg)

## Prerequisites

    qt5-base
    qt5-declarative
    qt5-graphicalffects
    qt5-quickcontrols2
    qtav

## Build

    cd openflixclientqt
    mkdir build
    cd build
    cmake ..
    cmake --build . --config Release --target all -- -j $(nproc)

## Run

    ./openflix

## Configuration


If you use a custom server instance, open config.conf and edit the value 
`providers` under the `CONTENT_CLIENT` section.