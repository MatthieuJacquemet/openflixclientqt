// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "utils.h"

#define PACKAGED_PEERFLIX 0
#define FETCH_PEERFLIX 1

#define HOST "localhost"
#define PORT 9000
#define PROTO "http"
#define KILL_TIMEOUT 4000
#define VIDEO_EXT {"mkv", "mp4", "avi", "flv", "mov"}

#if defined(Q_OS_LINUX)
    #define OS_SUFFIX "linux"
#elif defined(Q_OS_MACOS)
    #define OS_SUFFIX "macos"
#elif defined(Q_OS_WIN)
    #define OS_SUFFIX "win.exe"
#else
    #undef FETCH_PEERFLIX
#endif

#if defined(_X86_) || defined(i386) || defined(_M_IX86) 
    #define ARCH_SUFFIX "x86"
#elif defined(_M_X64) || defined(__x86_64__)
    #define ARCH_SUFFIX "x86_64"
#elif defined(__arm__) || defined(_M_ARM)
    #define ARCH_SUFFIX "arm"
#elif defined(__aarch64__)
    #define ARCH_SUFFIX "arm64"
#else
    #undef FETCH_PEERFLIX
#endif

#define EXE_BASENAME "peerflix-server"

#if FETCH_PEERFLIX
    #define FETCH_BASE_URL "https://gitlab.com/MatthieuJacquemet/storage/-/raw/master/bin/"
    #define PEERFLIX_EXE EXE_BASENAME "-" OS_SUFFIX "-" ARCH_SUFFIX
    #define FETCH_URL FETCH_BASE_URL PEERFLIX_EXE
#else
    #define PEERFLIX_EXE ""
    #define FETCH_BASE_URL
    #define FETCH_URL ""
#endif

#if PACKAGED_PEERFLIX
    #define PEERFLIX_PATH QStandardPaths::findExecutable("peerflix-server")
#else
    #define PEERFLIX_PATH ""
#endif

#define PEERFLIX_ARGS {}
#define CACHE_PATH QStandardPaths::writableLocation(QStandardPaths::CacheLocation)

#endif // __CONFIG_H__