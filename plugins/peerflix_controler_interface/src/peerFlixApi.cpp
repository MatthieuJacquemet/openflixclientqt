// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QNetworkReply>
#include <QtCore>

#include "peerFlixApi.h"
#include "path.h"
#include "openFlixApp.h"

using namespace std;


QNetworkAccessManager PeerFlixApi::manager;
quint16 PeerFlixApi::port = PORT;
QString PeerFlixApi::host = HOST;
QString PeerFlixApi::proto = PROTO;



QNetworkReply* PeerFlixApi::addTorrent(QString url) {

    
    QString path = TORRENTS;
    QUrlQuery query;
    Method method = Method::PostOperation;

    QJsonObject json;
    json.insert("link", QJsonValue::fromVariant(url));

    QJsonDocument doc(json);
    QByteArray data = doc.toJson();
    QNetworkRequest request;
    request.setRawHeader("Content-Type", "application/json");

    return call(path, query, method, data, qMove(request));
}


QNetworkReply* PeerFlixApi::torrent(QString info_hash) {

    return call(QString(TORRENT).arg(info_hash));
}


QNetworkReply* PeerFlixApi::delTorrent(QString info_hash) {

    QString path = QString(TORRENT).arg(info_hash);

    return call(path, QUrlQuery(), Method::DeleteOperation);
}


QUrl PeerFlixApi::baseUrl() {

    QUrl url;
    url.setScheme(proto);
    url.setHost(host);
    url.setPort(port);

    return url;
}


QNetworkReply* PeerFlixApi::doRequest(const QNetworkRequest& request,
                                    const Method method,
                                    const QByteArray& data)
{
    QNetworkReply* reply = nullptr;

    switch(method) {
        case Method::GetOperation:
            reply = manager.get(request); break;
        case Method::PostOperation:
            reply = manager.post(request, data); break;
        case Method::PutOperation:
            reply = manager.put(request, data); break;
        case Method::DeleteOperation:
            reply = manager.deleteResource(request); break;
        default:
            break;
    }

    return reply;
}


QJsonObject PeerFlixApi::readResponse(QNetworkReply* reply) {

    QJsonObject json;
    
    if (reply->error())
        qWarning() << reply->errorString();
    
    else {
        auto attrib = QNetworkRequest::HttpStatusCodeAttribute;
        QVariant status = reply->attribute(attrib);

        if (status.isValid()) {
            
            if (status.toInt() != 200) {
                auto attrib = QNetworkRequest::HttpReasonPhraseAttribute;
                qWarning() << reply->attribute(attrib);
            
            } else
                json = QJsonDocument::fromJson(reply->readAll()).object();
        }
    }
    reply->deleteLater();

    return json;
}


QNetworkReply* PeerFlixApi::call(const QString& path, const QUrlQuery& query, 
                                Method method, const QByteArray& data,
                                QNetworkRequest request) 
{
    QUrl url = baseUrl();
    url.setQuery(query);
    url.setPath(path);
    
    request.setUrl(url);

    return doRequest(request, method, data);
}