// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PEERFLIXAPI_H__
#define __PEERFLIXAPI_H__

#include <QtNetwork>

#include "config.h"
#include "paths.h"


class PeerFlixApi: public QObject {

    Q_OBJECT

public:
    using Method=QNetworkAccessManager::Operation;

    static QNetworkReply* addTorrent(QString url);
    static QNetworkReply* delTorrent(QString info_hash);
    static QNetworkReply* torrent(QString info_hash);
    static QUrl baseUrl();

    static QJsonObject readResponse(QNetworkReply* reply);

    static QNetworkReply* call(const QString& path,
                        const QUrlQuery& query=QUrlQuery(), 
                        Method method=Method::GetOperation, 
                        const QByteArray& data=QByteArray(),
                        QNetworkRequest request=QNetworkRequest());

    static QNetworkAccessManager manager;
    static quint16 port;
    static QString host;
    static QString proto;

private:
    static QNetworkReply* doRequest(const QNetworkRequest& request,
                            Method method=Method::GetOperation, 
                            const QByteArray& data=QByteArray());
};


#endif // __PEERFLIXAPI_H__