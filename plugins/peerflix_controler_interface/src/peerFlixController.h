// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PEERFLIXCONTROLLER_H__
#define __PEERFLIXCONTROLLER_H__

#include <QVariantMap>
#include <QVector>
#include <QProcess>

#include "contentAccess.h"

class QNetworkReply;


class PeerFlixController: public ContentAccess {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.openflix.access")
    Q_INTERFACES(ContentAccess)

public:
    PeerFlixController();
    virtual ~PeerFlixController();

    virtual void stopContent() override;
    virtual void exitContext() override;
    QVector<QString> getTypes() override;
    virtual void startContent(QVariantMap data) override;

private Q_SLOTS:
    void addTorrentHandler();
    void getTorrentHandler();
    void daemonFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void readStdout();
    void peerFlixDownloaded();

private:
    void prepareDaemon();
    void startDaemon();
    void stopDaemon(quint32 kill_timeout);
    void clearPeerFlixState();

    QVariantMap m_current_content;
    QProcess* m_daemon;
    QString m_peerflix_path;
    QStringList m_peerflix_args;
    QVector<QNetworkReply*> m_replies;
    QString m_current_info_hash;
    QMetaObject::Connection m_process_connection;
    QMetaObject::Connection m_stdout_coonection;
};

#endif // __PEERFLIXCONTROLLER_H__