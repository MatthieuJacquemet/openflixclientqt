// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QTimer>
#include <QNetworkReply>
#include <QProcess>

#include "openFlixApp.h"
#include "peerFlixApi.h"
#include "config.h"
#include "peerFlixController.h"

using namespace std;

using Api = PeerFlixApi;
using App = OpenFlixApp;



void PeerFlixController::prepareDaemon() {

    clearPeerFlixState();

    m_peerflix_path = config<QString>("peerflix/executable", PEERFLIX_PATH);
    m_peerflix_args = config<QStringList>("peerflix/args", PEERFLIX_ARGS);

    if (QFile(m_peerflix_path).exists())
        startDaemon();

#if FETCH_PEERFLIX

    else {
        QDir path = config<QString>("peerflix/download_dir", CACHE_PATH);
        m_peerflix_path = path.absoluteFilePath(PEERFLIX_EXE);
    
        if (QFile(m_peerflix_path).exists())
            startDaemon();
        else {
            QString url = config<QString>("peerflix/download_url", FETCH_URL);
            QNetworkRequest request(url);
            QNetworkReply* reply = Api::manager.get(request);

            connect(reply, SIGNAL(finished()), 
                this, SLOT(peerFlixDownloaded()));
        }  
    }
#endif
}


PeerFlixController::~PeerFlixController() {

    stopContent();
    stopDaemon(KILL_TIMEOUT);

    m_daemon->waitForFinished(4000);
}


QVector<QString> PeerFlixController::getTypes() {
    
    if (m_daemon->state() == QProcess::Running)
        return {"torrent", "magnet"};
    
    return {};
}


void PeerFlixController::startDaemon() {

    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("PORT", QString::number(Api::port));

    m_daemon = new QProcess();
    // m_daemon->setStandardOutputFile("peerflix.log", QIODevice::Append);
    m_daemon->setProcessChannelMode(QProcess::MergedChannels);
    m_daemon->setProgram(m_peerflix_path);
    m_daemon->setArguments(m_peerflix_args);
    m_daemon->setProcessEnvironment(env);
    
    m_process_connection = connect(m_daemon, 
        SIGNAL(finished(int, QProcess::ExitStatus)), this,
        SLOT(daemonFinished(int, QProcess::ExitStatus)));
    
    m_stdout_coonection = connect(m_daemon,
        SIGNAL(readyReadStandardOutput()), this, 
        SLOT(readStdout()));

    m_daemon->start();

    if(!m_daemon->waitForStarted())
        qWarning() << "failed to start peerflix daemon";
}


void PeerFlixController::clearPeerFlixState() {

    QDir dir(QStandardPaths::locate(QStandardPaths::HomeLocation, 
        ".config", QStandardPaths::LocateDirectory));

    dir.cd("peerflix-server");
    dir.remove("torrents.json");

    QString path = QStandardPaths::locate(QStandardPaths::TempLocation, 
                    "torrent-stream", QStandardPaths::LocateDirectory);

    if (!path.isEmpty())
        QDir(path).removeRecursively();
}


void PeerFlixController::daemonFinished(int exitCode, 
                                        QProcess::ExitStatus exitStatus)
{
    // hotfix because corrupted torrents.json may prevent peerflix-server from starting
    if (exitCode != QProcess::NormalExit) {
        clearPeerFlixState();
        // disconnect(m_process_connection); // no more try after this one
        m_daemon->start(); // retry to start peerflix server
    }
}


void PeerFlixController::readStdout() {

    QString pattern("Listening on http://localhost:(\\d+)");
    QRegularExpression re(pattern);

    QString output = m_daemon->readAll();
    QRegularExpressionMatch match = re.match(output);
        
    if (match.hasMatch()) {

        Api::port = match.captured(1).toUShort();
        disconnect(m_stdout_coonection);
    }
}


void PeerFlixController::peerFlixDownloaded() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    if (reply->error())
        qWarning() << reply->errorString();
    
    else {
        auto attrib = QNetworkRequest::HttpStatusCodeAttribute;
        QVariant status = reply->attribute(attrib);

        if (status.isValid()) {         
            if (status.toInt() != 200) {        
                auto attrib = QNetworkRequest::HttpReasonPhraseAttribute;
                qWarning() << reply->attribute(attrib);
            
            } else {
                QByteArray data = reply->readAll();
                QFile file(m_peerflix_path);
                file.open(QIODevice::WriteOnly);
                file.setPermissions(file.permissions() | QFileDevice::ExeUser);
                file.write(data);
                file.flush();
                file.close();

                startDaemon();
            }
        }
    }
    reply->deleteLater();
}


PeerFlixController::PeerFlixController():
    m_peerflix_path(PEERFLIX_PATH),
    m_peerflix_args(PEERFLIX_ARGS)
{
    prepareDaemon();
}


void PeerFlixController::stopDaemon(quint32 kill_timeout) {

    m_daemon->terminate();
    QTimer::singleShot(kill_timeout, m_daemon, &QProcess::kill);
}


void PeerFlixController::startContent(QVariantMap data=QVariantMap()) {

    if (data.isEmpty())
        data = m_current_content;
    else
        m_current_content = data;

    QString url = data["torrent"].toString();
    QNetworkReply* reply = Api::addTorrent(url);
    connect(reply, SIGNAL(finished()), this, SLOT(addTorrentHandler()));
    m_replies.push_back(reply);
}


void PeerFlixController::addTorrentHandler() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = Api::readResponse(reply);

    m_current_info_hash = json["infoHash"].toString();
    m_replies.removeOne(reply);
    reply = Api::torrent(m_current_info_hash);

    connect(reply, SIGNAL(finished()), this, SLOT(getTorrentHandler()));
    m_replies.push_back(reply);
}


bool is_video_file(QString name) {

    const QFileInfo info(name);
    const QStringList exts = VIDEO_EXT;

    return exts.contains(info.suffix(), Qt::CaseInsensitive);
}


void PeerFlixController::getTorrentHandler() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = Api::readResponse(reply);

    QJsonArray items_data = json["files"].toArray();

    QString file = reply->property("file").toString();

    App* app = App::globalInstance();

    if (items_data.isEmpty()) {
        clearPeerFlixState();

        QNetworkReply* rep = Api::delTorrent(m_current_info_hash);
        connect(rep, SIGNAL(finished()),this,SLOT(startContent(QVariantMap)));
    }

    for(const auto& item_data: items_data) {

        QJsonObject data = item_data.toObject();
        QString name = data["name"].toString();

        if (is_video_file(name) || name == file) {

            QString link = data["link"].toString();

            QUrl url = Api::baseUrl();
            url.setPath(link, QUrl::TolerantMode);

            app->contentReady(url.toString());
            break;
        }
    }
    m_replies.removeOne(reply);
}


void PeerFlixController::exitContext() {

    for (QNetworkReply* &reply: m_replies) {
        if (reply->isRunning())
            reply->abort();
    }
    m_replies.clear();
}


void PeerFlixController::stopContent() {

    if (!m_current_info_hash.isEmpty()) {

        QNetworkReply* reply = Api::delTorrent(m_current_info_hash);
        m_current_info_hash.clear();
        m_current_content.clear();
    }
}