#include <QDebug>
#include <QJsonDocument>
#include <QStandardPaths>
#include <QDir>
#include <QRandomGenerator>
#include <QMessageAuthenticationCode>
#include <QSysInfo>
#include <QDataStream>

#include "credentialVault.h"


QByteArray CredentialVault::readFile() {

    m_vault_file.reset();

#ifdef ENCRYPT_CREDENTIALS
    QByteArray iv = m_vault_file.read(16);

    QByteArray data = m_vault_file.readAll();
    data = m_crpyt.decode(data, m_cipher_key, iv);
    data = m_crpyt.removePadding(data);
    
    QByteArray mac = data.left(32);
    data = data.mid(32);

    QMessageAuthenticationCode code(QCryptographicHash::Sha256);
    code.setKey(m_cipher_key);
    code.addData(data);
    QByteArray expected_mac = code.result(); 

    if (mac != expected_mac)
        return QByteArray();

#else
    QByteArray data = m_vault_file.readAll();
#endif

    return data;
}


void CredentialVault::writeFile(QByteArray data) {

    m_vault_file.reset();

#ifdef ENCRYPT_CREDENTIALS
    QByteArray iv(16, Qt::Uninitialized);
    QRandomGenerator::system()->generate(iv.begin(), iv.end());

    QMessageAuthenticationCode code(QCryptographicHash::Sha256);
    code.setKey(m_cipher_key);
    code.addData(data);
    QByteArray mac = code.result();

    data.prepend(mac);
    data = m_crpyt.encode(data, m_cipher_key, iv);
    data.prepend(iv);
#endif

    m_vault_file.write(data);
    m_vault_file.flush();
}


void CredentialVault::save() {

    QByteArray data;

    QDataStream stream(&data, QIODevice::WriteOnly);
    stream << m_data;

    writeFile(data);
}


void CredentialVault::load() 
{
    QByteArray data = readFile();

    QDataStream stream(&data, QIODevice::ReadOnly);
    stream >> m_data;
}


void CredentialVault::setCredential(const QString& authority, 
    QAuthenticator* const auth)
{
    auto& data = m_data[authority];
    data.first = auth->user();
    data.second = auth->password();

    save();
}

bool CredentialVault::credentials(const QString& authority, 
                                QAuthenticator* const auth)
{    
    if (m_data.contains(authority)) {
        auto data = m_data[authority];
        auth->setUser(data.first);
        auth->setPassword(data.second);
        return true;
    }

    return false;
}


CredentialVault::CredentialVault(QString path):
    m_vault_file(path)
#ifdef ENCRYPT_CREDENTIALS
    , m_crpyt(QAESEncryption::AES_256, QAESEncryption::CFB, QAESEncryption::PKCS7)
#endif
{
    m_cipher_key = QSysInfo::machineUniqueId();

    if (m_vault_file.open(QIODevice::ReadWrite))
        load();
}


CredentialVault::~CredentialVault() {

    save();
    m_vault_file.close();
}