// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QApplication>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLabel>
#include <QDebug>

#include "authDialog.h"



AuthDialog::AuthDialog(QAuthenticator* auth):
    m_auth(auth)
{

    // QWidget* window = new QWidget();
    // window->setWindowTitle(
        // QApplication::translate("toplevel", auth-> ));
    // setWindowFlags(Qt::Dialog);
    resize(300,200);
    QLabel* realm = new QLabel();
    realm->setText(auth->realm());

    QDialogButtonBox* buttons = new QDialogButtonBox();
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);
    buttons->button(QDialogButtonBox::Ok)->setText(tr("Login"));
    buttons->button(QDialogButtonBox::Cancel)->setText(tr("Cancer"));

    QVBoxLayout* input_layout = new QVBoxLayout();
    m_username_field = new QLineEdit();
    m_password_field = new QLineEdit();
    m_username_field->setPlaceholderText(tr("username"));
    m_password_field->setPlaceholderText(tr("password"));

    input_layout->addWidget(m_username_field);
    input_layout->addWidget(m_password_field);

    m_password_field->setEchoMode(QLineEdit::Password);
    // QHBoxLayout* button_layout = new QHBoxLayout();
    // QPushButton* ok_button = new QPushButton();
    // QPushButton* close_button = new QPushButton();
    // button_layout->addWidget(ok_button);
    // button_layout->addWidget(close_button);

    connect(buttons->button(QDialogButtonBox::Cancel),
        SIGNAL(clicked()), this, SLOT(cancelHandler())
    );

    connect(buttons->button(QDialogButtonBox::Ok), 
        SIGNAL(clicked()), this, SLOT(loginHandler())
    );

    QVBoxLayout* main_layout = new QVBoxLayout();
    main_layout->addWidget(realm);
    main_layout->addLayout(input_layout);
    main_layout->addWidget(buttons);

    setLayout(main_layout);
}

void AuthDialog::loginHandler() {

    m_auth->setUser(m_username_field->text());
    m_auth->setPassword(m_password_field->text());
    accept();
}

void AuthDialog::cancelHandler() {

    // m_auth->setUser(m_username_field->text());
    // m_auth->setPassword(m_password_field->text());
    close();
}