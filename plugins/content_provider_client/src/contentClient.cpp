#include <iostream>
#include <QQuickItem>

#include "contentApi.h"
#include "utils.h"
#include "config.h"
#include "contentClient.h"


using namespace std;

using Api = ContentApi;
using App = OpenFlixApp;



void ContentClient::contentHandler() {

    App* app = App::globalInstance();

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = Api::readResponse(reply);
    QJsonArray items_data = json["results"].toArray();
    
    for(const auto& item_data: items_data) {
        
        Content content;

        QJsonObject data = item_data.toObject();
        content.name = data["name"].toString();
        content.provider = data["provider"].toString();
        content.size = data["size"].toDouble();
        content.type = data["type"].toString();
        content.data = data["data"].toObject().toVariantMap(); 

        app->addContent(content);
    }

    m_replies.removeOne(reply);
}


void ContentClient::authHandler(QNetworkReply* reply, QAuthenticator* auth) {

    QString username = auth->user();
    QString password = auth->password();
    QString authority = reply->url().authority();

    if (!m_vault.credentials(authority, auth) ||
        (auth->user() == username && auth->password() == password))
    {
        AuthDialog dialog(auth);
        dialog.setWindowTitle(authority);
        dialog.show();

        if (dialog.exec() == QDialog::Accepted)
            m_vault.setCredential(authority, auth);
        else
            reply->abort();
    }
}


ContentClient::ContentClient():
    m_vault(config<QString>("client/credential_file", VAULT_FILE))
{

    connect(&Api::manager, 
        SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)),
        this, SLOT(authHandler(QNetworkReply*, QAuthenticator*)));
    qCritical() << "keys" << OpenFlixApp::globalInstance()->settings.allKeys();
    m_servers = config<QStringList>("client/providers");
}


void ContentClient::content(const ContentDesc& desc) {

    QString lang = desc.language;

    if (lang.isEmpty())
        lang = OpenFlixApp::globalInstance()->locale.bcp47Name();
    qCritical() << "request content";
    for (const QString& server: m_servers) {
        qCritical() << "server content " << server;
        auto reply = Api::content(  desc.title, desc.original_title, 
                                    desc.date, lang, server, Api::movie);

        connect(reply, SIGNAL(finished()),this, SLOT(contentHandler()));
        m_replies.push_back(reply);
    }
}

void ContentClient::exitContext() {

    Q_EMIT closeDialog();

    for (QNetworkReply* &reply: m_replies) {
        if (reply->isRunning())
            reply->abort();
    }
    m_replies.clear();
}
