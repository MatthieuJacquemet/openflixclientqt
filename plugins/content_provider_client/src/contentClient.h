// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONTENTCLIENT_H__
#define __CONTENTCLIENT_H__

#include "contentProvider.h"
#include "contentApi.h"
#include "credentialVault.h"
#include "authDialog.h"


class ContentClient: public ContentProvider {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.openflix.content")
    Q_INTERFACES(ContentProvider)

public:
    ContentClient();
    virtual ~ContentClient() = default;

    virtual void content(const ContentDesc& desc) override;
    virtual void exitContext() override;

private Q_SLOTS:
    void contentHandler();
    void authHandler(QNetworkReply * reply, QAuthenticator* auth);

Q_SIGNALS:
    void closeDialog();

private:
    CredentialVault m_vault;
    QVector<QNetworkReply*> m_replies;
    QStringList m_servers;
    QAuthenticator m_prev_auth;
};


#endif // __CONTENTCLIENT_H__