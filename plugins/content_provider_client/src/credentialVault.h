// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CREDENTIALVAULT_H__
#define __CREDENTIALVAULT_H__

#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QAuthenticator>
#include <QJsonObject>
#include <QMap>
#include <QFile>

#include "config.h"
#include "client_config.h"

#ifdef ENCRYPT_CREDENTIALS
    #include "qaesencryption.h"
#endif


class CredentialVault : public QObject {

    Q_OBJECT

public:
    CredentialVault(QString path);
    ~CredentialVault();

    void setCredential(const QString& authority, QAuthenticator* const auth);
    bool credentials(const QString& authority, QAuthenticator* const auth);

private:
    QFile m_vault_file;
    QMap<QString, QPair<QString, QString>>  m_data;
    QByteArray m_cipher_key;

#ifdef ENCRYPT_CREDENTIALS
    QAESEncryption m_crpyt;
#endif

    void save();
    void load();

    QByteArray readFile();
    void writeFile(QByteArray data);
};

#endif // __CREDENTIALVAULT_H__