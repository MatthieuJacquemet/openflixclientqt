// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __AUTHDIALOG_H__
#define __AUTHDIALOG_H__


#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QAuthenticator>


class AuthDialog : public QDialog {

    Q_OBJECT

public:
    AuthDialog(QWidget* parent = nullptr);
    AuthDialog(QAuthenticator* auth);

private Q_SLOTS:
    void loginHandler();
    void cancelHandler();
// Q_SIGNALS:
//     void login(QString username, QString password, QAuthenticator* auth);

private:
    QLineEdit* m_username_field;
    QLineEdit* m_password_field;
    QAuthenticator* m_auth;
    // QAuthenticator* m_auth;
};
#endif // __AUTHDIALOG_H__