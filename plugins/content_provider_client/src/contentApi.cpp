// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QtPlugin>
#include <QtCore>

#include "client_config.h"
#include "utils.h"
#include "contentApi.h"
#include "openFlixApp.h"

using namespace std;


QNetworkAccessManager ContentApi::manager;


void ContentApi::setup() {

    QNetworkDiskCache *cache = new QNetworkDiskCache(&manager);

    QString cache_path = config<QString>("api/cache_path", CACHE_PATH);
    qint64 cache_size = config<qint64>("api/cache_size", CACHE_SIZE);

    cache->setCacheDirectory(cache_path);
    cache->setMaximumCacheSize(cache_size);
    
    manager.setCache(cache);
}



QNetworkReply* ContentApi::content( const QString& title,
                                    const QString& original_title,
                                    const QString& date,
                                    const QString& language,
                                    const QString& authority,
                                    MediaType type) 
{
    QMetaEnum types_info = staticMetaObject.enumerator(0);
    QUrlQuery query;

    query.addQueryItem("title", title);
    query.addQueryItem("original_title", original_title);
    query.addQueryItem("date", date);
    query.addQueryItem("language", language);
    query.addQueryItem("type", types_info.valueToKey(type));
    
    return call(CONTENT, authority, query, Method::GetOperation, Q_NULLPTR);
}


QNetworkReply* ContentApi::doRequest(const QNetworkRequest& request,
                                    Method method, QHttpMultiPart* data)
{
    QNetworkReply* reply = Q_NULLPTR;

    switch(method) {
        case Method::GetOperation:
            reply = manager.get(request); break;
        case Method::PostOperation:
            reply = manager.post(request, data); break;
        case Method::PutOperation:
            reply = manager.put(request, data); break;
        case Method::DeleteOperation:
            reply = manager.deleteResource(request); break;
        default: break;
    }  
    return reply;
}

QJsonObject ContentApi::readResponse(QNetworkReply* reply) {

    QJsonObject json;
    
    if (reply->error())
        qWarning() << reply->errorString();
    
    else {
        auto attrib = QNetworkRequest::HttpStatusCodeAttribute;
        QVariant status = reply->attribute(attrib);

        if (status.isValid()) {
            
            if (status.toInt() != 200) {
                auto attrib = QNetworkRequest::HttpReasonPhraseAttribute;
                qWarning() << reply->attribute(attrib);
            
            } else
                json = QJsonDocument::fromJson(reply->readAll()).object();
        }
    }
    reply->deleteLater();

    return json;
}

QNetworkReply* ContentApi::call(const QString& path,
                                const QString& authority,
                                const QUrlQuery& query,
                                Method method, QHttpMultiPart* multipart) 
{
    QUrl url;
    url.setScheme("https");
    url.setAuthority(authority);
    url.setQuery(query);
    url.setPath(path);

    QNetworkRequest request(url);

    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, 
                        QNetworkRequest::PreferCache);

    // request.setAttribute(QNetworkRequest::AuthenticationReuseAttribute,
    //                     QNetworkRequest::Manual);

    return doRequest(request, method, multipart);
}

