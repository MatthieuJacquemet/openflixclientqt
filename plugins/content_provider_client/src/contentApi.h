// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONTENTAPI_H__
#define __CONTENTAPI_H__

#include <QtNetwork>

#include "config.h"
#include "paths.h"


class ContentApi: public QObject {

    Q_OBJECT

public:
    using Method=QNetworkAccessManager::Operation;

    enum MediaType {movie, music, show, Invalid};
    Q_ENUM(MediaType)
    
    static void setup();

    static QJsonObject readResponse(QNetworkReply* reply);

    static QNetworkReply* content(  const QString& title,
                                    const QString& original_title,
                                    const QString& date,
                                    const QString& language,
                                    const QString& authority,
                                    ContentApi::MediaType type);

    static QNetworkReply* call( const QString& path, 
                                const QString& authority,
                                const QUrlQuery& query, 
                                Method method=Method::GetOperation, 
                                QHttpMultiPart* multipart=Q_NULLPTR);


    static QNetworkAccessManager manager;

private:
    static QNetworkReply* doRequest(const QNetworkRequest& req,
                            Method method=Method::GetOperation, 
                            QHttpMultiPart* data=Q_NULLPTR);
};


#endif // __CONTENTAPI_H__