#include "qml_plugin.h"
#include "config.h"

void QmlGui::init() {

    QQmlContext* context = m_engine->rootContext();

    context->setContextProperty("app", this);

    m_engine->load(URI_MAIN_QML);

    if (m_engine->rootObjects().isEmpty())
        exit(EXIT_FAILURE);
}

QQmlApplicationEngine* QmlGui::get_engine() {

    return m_engine;
}

void QmlGui::set_engine(QQmlApplicationEngine* engine) {

    m_engine = engine;
}