#pragma once

#include <QQmlApplicationEngine>
#include "plugin.h"

class QmlGui: public OpenFlixGui {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.openflix.plugin" FILE "qml_gui.json")
    Q_INTERFACES(OpenFlixGui)

public:

    void init() override;
    QQmlApplicationEngine* get_engine();
    void set_engine(QQmlApplicationEngine* engine);

private:

    QQmlApplicationEngine* m_engine;
};
