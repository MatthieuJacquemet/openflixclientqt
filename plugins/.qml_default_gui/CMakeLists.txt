cmake_minimum_required(VERSION 3.1)

project(qml-gui LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Core Quick Widgets REQUIRED)
find_package(VLCQt COMPONENTS Qml Core REQUIRED)

set(SRC_DIR src)
file(GLOB_RECURSE SRCS ${SRC_DIR}/*.cpp)

add_library(${PROJECT_NAME} SHARED ${SRCS})

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_17)
set(OPENFLIX_PLUGINS ${OPENFLIX_PLUGINS} ${PROJECT_NAME} PARENT_SCOPE)

target_compile_definitions(${PROJECT_NAME} PUBLIC
    $<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)

target_link_libraries(${PROJECT_NAME} PRIVATE Qt5::Core
    Qt5::Quick Qt5::Widgets VLCQt::Qml pthread)