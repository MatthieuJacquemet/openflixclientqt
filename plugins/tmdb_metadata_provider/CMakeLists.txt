cmake_minimum_required(VERSION 3.13)

project(tmdb-provider LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(QT_VERSION_MINOR 11)

find_package(Qt5 COMPONENTS Core Quick Widgets REQUIRED)

set(SRC_DIR src)
file(GLOB_RECURSE SRCS ${SRC_DIR}/*.cpp)

add_library(${PROJECT_NAME} STATIC ${SRCS})

set(OPENFLIX_PLUGINS ${OPENFLIX_PLUGINS} ${PROJECT_NAME} PARENT_SCOPE)


target_compile_definitions(${PROJECT_NAME} PUBLIC
$<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:QT_QML_DEBUG>)

target_compile_definitions(${PROJECT_NAME} PRIVATE QT_STATICPLUGIN)
    target_link_libraries(openflix PRIVATE ${PROJECT_NAME})

target_link_libraries(${PROJECT_NAME} PRIVATE
    Qt5::Core Qt5::Quick Qt5::Widgets)

    