// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __TMDBAPI_H__
#define __TMDBAPI_H__


#include <QtNetwork>

#include "tmdb_path.h"
#include "tmdb_config.h"


class TmdbApi: public QObject {

    Q_OBJECT

public:
    using Method=QNetworkAccessManager::Operation;

    enum MediaType {all, movie, tv, person, InvalidType};
    Q_ENUM(MediaType)

    enum TimeWindow {day, week, InvalidTime};
    Q_ENUM(TimeWindow)

    static void setup();

    static QString posterUrl(const QString& path, const QString& size="w185");
    static QString backdropUrl(const QString& path, const QString& size="original");

    static QNetworkReply* trending(MediaType type, 
                            TimeWindow window=day, quint16 page=1);
    
    static QNetworkReply* details(quint32 id, MediaType type);

    static QNetworkReply* search(const QString& name, MediaType type, quint16 page);

    static QJsonObject readResponse(QNetworkReply* reply);

    static QNetworkReply* call(const QString& path, QUrlQuery query=QUrlQuery(),
                            Method method=Method::GetOperation, 
                            QHttpMultiPart* multipart=Q_NULLPTR);

    static QNetworkAccessManager manager;

private:
    static QNetworkReply* doRequest(const QNetworkRequest& req,
                                    Method method=Method::GetOperation, 
                                    QHttpMultiPart* data=Q_NULLPTR);

    static QString language;
    static QStringList poster_sizes;
    static QStringList backdrop_sizes;
    static QString img_base_url;
    static QString token;
    static QString authority;
    static qint8 api_version;
    static qint32 remaining;
    static qint32 reset;
    static bool sleep_on_rate_limit;
};

#endif // __TMDBAPI_H__