// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QtCore>

#include "utils.h"
#include "tmdbApi.h"


using namespace std;


QNetworkAccessManager TmdbApi::manager;
QString TmdbApi::language;
QString TmdbApi::img_base_url = IMAGE_BASE_URL;
QStringList TmdbApi::poster_sizes;
QStringList TmdbApi::backdrop_sizes;
QString TmdbApi::token = TMTD_TOKEN;
QString TmdbApi::authority = TMDB_AUTHORITY;
qint8 TmdbApi::api_version = 3;
qint32 TmdbApi::remaining = 40;
qint32 TmdbApi::reset = 0;
bool TmdbApi::sleep_on_rate_limit = true;



void TmdbApi::setup() {

    QNetworkDiskCache *cache = new QNetworkDiskCache(&manager);

    QString cache_path = config<QString>("api/cache_path", CACHE_PATH);
    qint64 cache_size = config<qint64>("api/cache_size", CACHE_SIZE);

    cache->setCacheDirectory(cache_path);
    cache->setMaximumCacheSize(cache_size);
    
    manager.setCache(cache);

    auto reply = call(CONFIGURATION);

    QObject::connect(reply, &QNetworkReply::finished, [reply] {

        QJsonObject json = readResponse(reply);

        QJsonObject image = json["images"].toObject();
        QJsonArray po_sizes = image["poster_sizes"].toArray();
        QJsonArray bd_sizes = image["backdrop_sizes"].toArray();

        img_base_url = image["base_url"].toString();
        
        for (const auto& size: po_sizes)
            poster_sizes.append(size.toString());

        for (const auto& size: bd_sizes)
            backdrop_sizes.append(size.toString());
    });
}


QString TmdbApi::posterUrl(const QString& path, const QString& size) {

    return img_base_url + size + path;
}


QString TmdbApi::backdropUrl(const QString& path, const QString& size) {

    return img_base_url + size + path;
}


QNetworkReply* TmdbApi::trending(MediaType type, 
                                TimeWindow window,
                                quint16 page)
{

    QUrlQuery query;
    query.addQueryItem("page" ,QString::number(page));

    QMetaEnum enum0 = staticMetaObject.enumerator(0);
    QMetaEnum enum1 = staticMetaObject.enumerator(1);

    QString media = enum0.valueToKey(type);
    QString time = enum1.valueToKey(window);

    QString path = QString(TRENDING).arg(media,time);

    return call(path, query);
}


QNetworkReply* TmdbApi::search(const QString& name, MediaType type, quint16 page) {

    QUrlQuery query;
    query.addQueryItem("query", name);
    query.addQueryItem("page" ,QString::number(page));

    QMetaEnum enum0 = staticMetaObject.enumerator(0);
    QString media = enum0.valueToKey(type);

    return call(QString(SEARCH).arg(media), query);
}


QNetworkReply* TmdbApi::details(quint32 id, MediaType type) {

    QMetaEnum enum0 = staticMetaObject.enumerator(0);
    QString media = enum0.valueToKey(type);

    return call(QString("%1/%2").arg(media).arg(id));
}


QNetworkReply* TmdbApi::doRequest( const QNetworkRequest& request,
                                    Method method, QHttpMultiPart* data)
{
    QNetworkReply* reply = nullptr;

    switch(method) {
        case Method::GetOperation:
            reply = manager.get(request); break;
        case Method::PostOperation:
            reply = manager.post(request, data); break;
        case Method::PutOperation:
            reply = manager.put(request, data); break;
        case Method::DeleteOperation:
            reply = manager.deleteResource(request); break;
        default: break;
    }  
    return reply;
}


QJsonObject TmdbApi::readResponse(QNetworkReply* reply) {

    QJsonObject json;

    if (reply->error())
        qWarning() << reply->errorString();
    
    else {
        QByteArray limit_rem = reply->rawHeader("X-RateLimit-Remaining");
        QByteArray limit_res = reply->rawHeader("X-RateLimit-Reset");


        if (!limit_rem.isEmpty())
            remaining = limit_rem.toInt();

        if (!limit_res.isEmpty())
            reset = limit_res.toInt();

        if (remaining < 1) {
            qint64 current_time = QDateTime().toSecsSinceEpoch();
            qint64 sleep_time = reset - current_time;

            if (sleep_on_rate_limit) {
                // QThread::sleep(abs(sleep_time));
                
                // this->do_request(reply->request(), reply->operation());
                
            } else {
                QString msg("Rate limit reached. Try again in %1 seconds");
                qWarning() << msg.arg(sleep_time);
            }
        }

        auto attrib = QNetworkRequest::HttpStatusCodeAttribute;
        QVariant status = reply->attribute(attrib);

        if (status.isValid()) {
            
            if (status.toInt() != 200) {
                auto attrib = QNetworkRequest::HttpReasonPhraseAttribute;
                qWarning() << reply->attribute(attrib);
            
            } else {
                json = QJsonDocument::fromJson(reply->readAll()).object();
                // qDebug() << "loaded from cache" << reply->attribute(QNetworkRequest::SourceIsFromCacheAttribute).toBool();
                // auto handler = get_property<SignalHandler*>(reply, "_handler");

                // if (handler != nullptr)
                //     handler->call(json);

                // _handler(reply->readAll(), _this);
            }
        }
    }
    reply->deleteLater();

    return json;
}


QNetworkReply* TmdbApi::call(  const QString& path,
                                QUrlQuery query,
                                Method method,
                                QHttpMultiPart* multipart) 
{
    if (token.isEmpty())
        qWarning("No API key found");
    
    query.addQueryItem("api_key", token);
    query.addQueryItem("language", getLanguage());

    QUrl url;
    url.setScheme("http");
    url.setAuthority(authority);
    url.setQuery(query);
    url.setPath(QString("/%1/%2").arg(api_version).arg(path));
    
    QNetworkRequest request(url);

    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, 
                        QNetworkRequest::PreferCache);

    return doRequest(request, method, multipart);
}