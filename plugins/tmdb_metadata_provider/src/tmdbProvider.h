// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __TMDBPROVIDER_H__
#define __TMDBPROVIDER_H__

#include "metaDataProvider.h"

class QNetworkReply;


class TmdbProvider: public MetaDataProvider {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.openflix.metadata")
    Q_INTERFACES(MetaDataProvider)

public:
    TmdbProvider();
    virtual ~TmdbProvider() = default;

    virtual void fetchMore() override;
    virtual void getDetails(int id) override;
    virtual void search(const QString& name) override;
    virtual void exitContext() override;

private Q_SLOTS:

    void fetchHandler();
    void detailsHandler();
    void clearlogoHandler();
    void updateContext();

private:
    quint16 m_page;
    quint8 m_context;
    QString m_search;
    QVector<QNetworkReply*> m_replies;
    bool m_no_more;
};

#endif // __TMDBPROVIDER_H__