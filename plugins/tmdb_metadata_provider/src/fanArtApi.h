// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __FANARTAPI_H__
#define __FANARTAPI_H__

#include <QtNetwork>

#include "fanart_path.h"
#include "tmdb_config.h"


class FanArtApi: public QObject {

    Q_OBJECT

public:
    using Method=QNetworkAccessManager::Operation;

    enum MediaType {movies, music, tv, Invalid};
    Q_ENUM(MediaType)

    static void setup();

    static QNetworkReply* images(quint32 id, MediaType type);

    static QJsonObject readResponse(QObject* sender);

    static QNetworkReply* call( const QString& path,
                                QUrlQuery query=QUrlQuery(), 
                                Method method=Method::GetOperation, 
                                QHttpMultiPart* multipart=Q_NULLPTR);

    static QNetworkAccessManager manager;
    static qint8 api_version;
    static QString language;
    static QString token;
    static QString authority;

private:
    static QNetworkReply* doRequest(const QNetworkRequest& req,
                                    Method method=Method::GetOperation, 
                                    QHttpMultiPart* data=Q_NULLPTR);

};

#endif // __FANARTAPI_H__