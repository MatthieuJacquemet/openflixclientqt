// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QtCore>

#include "utils.h"
#include "fanArtApi.h"

using namespace std;


QNetworkAccessManager FanArtApi::manager;
qint8 FanArtApi::api_version = 3;
QString FanArtApi::language;
QString FanArtApi::token = FANART_TOKEN;
QString FanArtApi::authority = FANART_AUTHORITY;


void FanArtApi::setup() {

    QNetworkDiskCache* cache = new QNetworkDiskCache(&manager);

    QString cache_path = config<QString>("api/cache_path", CACHE_PATH);
    qint64 cache_size = config<qint64>("api/cache_size", CACHE_SIZE);

    cache->setCacheDirectory(cache_path);
    cache->setMaximumCacheSize(cache_size);
    
    manager.setCache(cache);
}


QNetworkReply* FanArtApi::images(quint32 id, FanArtApi::MediaType type) {

    QMetaEnum info = staticMetaObject.enumerator(0);
    QString type_name = info.valueToKey(type);

    return call(QString("%1/%2").arg(type_name).arg(id));
}


QNetworkReply* FanArtApi::doRequest(const QNetworkRequest& request,
                                    Method method, QHttpMultiPart* data)
{
    QNetworkReply* reply = nullptr;

    switch(method) {
        case Method::GetOperation:
            reply = manager.get(request); break;
        case Method::PostOperation:
            reply = manager.post(request, data); break;
        case Method::PutOperation:
            reply = manager.put(request, data); break;
        case Method::DeleteOperation:
            reply = manager.deleteResource(request); break;
        default: break;
    }  
    return reply;
}


QJsonObject FanArtApi::readResponse(QObject* sender) {

    QJsonObject json;
    
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender);

    if (reply->error())
        qWarning() << reply->errorString();
    
    else {
        auto attrib = QNetworkRequest::HttpStatusCodeAttribute;
        QVariant status = reply->attribute(attrib);

        if (status.isValid()) {
            
            if (status.toInt() != 200) {
                auto attrib = QNetworkRequest::HttpReasonPhraseAttribute;
                qWarning() << reply->attribute(attrib);
            
            } else
                json = QJsonDocument::fromJson(reply->readAll()).object();
        }
    }
    reply->deleteLater();

    return json;
}

QNetworkReply* FanArtApi::call( const QString& path, QUrlQuery query, 
                                Method method, QHttpMultiPart* multipart) 
{
    if (token.isEmpty())
        qWarning("No API key found");
    
    query.addQueryItem("api_key", token);

    QUrl url;
    url.setScheme("http");
    url.setAuthority(authority);
    url.setQuery(qMove(query));
    url.setPath(QString("/v%1/%2").arg(api_version).arg(path));
    
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::CacheLoadControlAttribute, 
                        QNetworkRequest::PreferCache);

    return doRequest(request, method, multipart);
}