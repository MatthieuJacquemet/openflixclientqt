// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include <QQuickItem>

#include "tmdbApi.h"
#include "fanArtApi.h"

#include "utils.h"
#include "config.h"
#include "tmdbProvider.h"


using namespace std;

using Api = TmdbApi;
using FanArt = FanArtApi;
using App = OpenFlixApp;



TmdbProvider::TmdbProvider():
    m_context(OpenFlixApp::CTX_TRENDING),
    m_page(1),
    m_no_more(false)
{
    TmdbApi::setup();
    FanArtApi::setup();
}


void TmdbProvider::fetchHandler() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = Api::readResponse(reply);

    App* app = App::globalInstance();

    QJsonArray items_data = json["results"].toArray();
    int total_results = json["total_results"].toInt();
    
    QString search = reply->property("search").toString();

    if (search != m_search)
        goto end;
    
    if (total_results == 0) {
        app->noResults();
        goto end;
    }

    if (items_data.isEmpty())
        m_no_more = true;
    else {
        for(const auto& item_data: items_data) {
            
            QJsonObject data = item_data.toObject();

            QString poster_path = data["poster_path"].toString();
            QString date = data["release_date"].toString();

            ItemData item;
            item.poster = Api::posterUrl(poster_path,"w300");
            item.title = data["title"].toString();
            item.year = date.split("-").at(0);
            item.id = data["id"].toInt();

            app->appendGridItem(item);
            // Q_EMIT appendGridItem(title, year, poster_url, id);
        }
    }
end:
    m_replies.removeOne(reply);
}

void TmdbProvider::detailsHandler() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = Api::readResponse(reply);
    
    App* app = App::globalInstance();

    QString poster_path = json["poster_path"].toString();
    QString backdrop_path = json["backdrop_path"].toString();

    MediaDetails details;

    Q_EMIT app->setDetails( json["title"].toString(),
                            json["original_title"].toString(),
                            json["release_date"].toString(),
                            json["overview"].toString(),
                            Api::posterUrl(poster_path, "w300"),
                            Api::backdropUrl(backdrop_path),
                            json["overview"].toString());

    m_replies.removeOne(reply);
}


void TmdbProvider::clearlogoHandler() {

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonObject json = FanArt::readResponse(reply);
    QJsonArray items_data = json["hdmovielogo"].toArray();
    QString ui_language = getLanguage().split("-").at(0);
    QString url = "";
    QString url_en = "";

    App* app = App::globalInstance();

    for(const auto& item_data: items_data) {
    
        QJsonObject data = item_data.toObject();
        QString language = data["lang"].toString();

        if (language == ui_language) {
            url = data["url"].toString();
            break;
        } else if (language == "en" && url_en.isEmpty())
            url_en = data["url"].toString();
    }
    
    if (url.isEmpty())
        url = url_en;
    
    if (url.isEmpty() && !items_data.empty()) {
        QJsonObject data = items_data.at(0).toObject();
        url = data["url"].toString();
    }
    
    // Q_EMIT setClearLogo(url);
    app->setClearLogo(url);

    m_replies.removeOne(reply);
}


void TmdbProvider::updateContext() {
    
}


void TmdbProvider::getDetails(int id) {

    auto reply = Api::details(id, Api::movie);
    connect(reply, SIGNAL(finished()),this, SLOT(detailsHandler()));
    m_replies.push_back(reply);

    reply = FanArt::images(id, FanArt::movies);
    connect(reply, SIGNAL(finished()),this, SLOT(clearlogoHandler()));
    m_replies.push_back(reply);
}


void TmdbProvider::search(const QString& name) {

    m_page = 1;
    
    if (name != m_search)
        m_no_more = false;
    
    m_search = name;
}

void TmdbProvider::fetchMore() {

    if (m_no_more)
        return;

    App* app = App::globalInstance();

    QNetworkReply* reply;

    Api::MediaType type = Api::movie;
    Api::TimeWindow time = Api::day;

    if (m_search.isEmpty())
        reply = Api::trending(type, time, m_page);
    else {
        reply = Api::search(m_search, type, m_page);
        reply->setProperty("search", m_search);
    }

    connect(reply, SIGNAL(finished()), this, SLOT(fetchHandler()));
    m_replies.push_back(reply);
    m_page++;
}

void TmdbProvider::exitContext() {

    for (QNetworkReply* &reply: m_replies) {
        if (reply->isRunning())
            reply->abort();
    }
    m_replies.clear();
}

