#!/usr/bin/env sh

if [ -f "$1" ]; then
    sed -i "s/\.tmpDir()/.tmpdir()/" "$1"
else
    echo "file not found"
fi