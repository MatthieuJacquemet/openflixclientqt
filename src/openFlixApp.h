// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __OPENFLIXAPP_H__
#define __OPENFLIXAPP_H__


#include <QApplication>
#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QTranslator>
#include <QSettings>
#include <QQuickItem>

#include "medias.h"
#include "content.h"

class PluginManager;
class OpenFlixPlugin;


#ifdef BUILDING_OPENFLIX
    #define LIB_EXPORT Q_DECL_EXPORT
#else
    #define LIB_EXPORT Q_DECL_IMPORT
#endif



class LIB_EXPORT OpenFlixApp: public QApplication {

    Q_OBJECT

    Q_PROPERTY(MediaManager* itemModel READ itemModel NOTIFY itemsChanged)
    Q_PROPERTY(ContentManager* contentModel READ contentModel NOTIFY contentsChanged)

public:
    OpenFlixApp(int argc, char *argv[]);
    virtual ~OpenFlixApp() = default;

    Q_DISABLE_COPY(OpenFlixApp)

    static OpenFlixApp* globalInstance();
    static QString relativePath(const QString& name);

    void parseOptions();
    void loadEngine();
    void setup();


    void appendGridItem(const QString& title,
                        const QString& year="", 
                        const QString& poster_url="", int id=0);
    
    void appendGridItem(const ItemData& item);
    void addContent(const Content& content);

    MediaManager* itemModel();
    ContentManager* contentModel();

    enum Media {
        MOVIE = 0,
        TV_SHOW
    };
    Q_ENUM(Media)

    enum Context {
        CTX_TRENDING = 0,
        CTX_SEARCH
    };
    Q_ENUM(Context)

    QSettings settings;
    QCommandLineParser parser;
    QLocale locale;

private:
    QTranslator m_translator;
    QQmlApplicationEngine m_engine;
    MediaManager m_items_model;
    ContentManager m_contents_model;

public Q_SLOTS:
    Q_INVOKABLE void fetchMoreItems();
    Q_INVOKABLE void exitContext();
    Q_INVOKABLE void getDetails(int id);
    Q_INVOKABLE void search(const QString& name);
    Q_INVOKABLE void startContent(  const QString& type,
                                    const QVariantMap& data);
    Q_INVOKABLE void stopContent();
    Q_INVOKABLE void content(   const QString& title,
                                const QString& original_title, 
                                const QString& year,
                                const QString& language="");

Q_SIGNALS:
    void itemsChanged();
    void contentsChanged();
    void noResults();
    void setClearLogo(const QString& url);

    void setDetails(   const QString& title,
                        const QString& original_title,
                        const QString& date, 
                        const QString& overview,
                        const QString& poster,
                        const QString& backdrop,
                        const QString& synopsis);
   
    void contentReady(const QString& uri); 
};

#endif // __OPENFLIXAPP_H__