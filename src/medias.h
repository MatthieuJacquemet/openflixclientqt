#pragma once

#include <QString>
#include <QAbstractListModel>

struct ItemData {

    QString title;
    QString year;
    QString poster;
    quint64 id;
};

struct MediaDetails {

    QString title;
    QString original_title;
    QString overview;
    QString date;
    QString poster;
    QString backdrop;
    QString synopsis;
};

class MediaManager: public QAbstractListModel {

    Q_OBJECT

public:
    enum GridItemRoles {
        TitleRole = 0,
        YearRole,
        PosterRole,
        IdRole
    };

    explicit MediaManager(QObject* parent = nullptr);
    virtual ~MediaManager() = default;

    virtual QVariant data(  const QModelIndex &index,
                            int role=Qt::DisplayRole) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;
    
    void addItem(const ItemData& data);
    Q_INVOKABLE void clear();

private:
    QVector<ItemData> m_items;
};