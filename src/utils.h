// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __UTILS_H__
#define __UTILS_H__


#include <QMetaEnum>
#include <qglobal.h>
#include <QString>



#define CAT(A, B) A ## B
#define MACRO_CONCAT(a, b) CAT(a,b)
#define SELECT(NAME, NUM) CAT(NAME ## _, NUM)
#define COMPOSE(NAME, ARGS) NAME ARGS

#define GET_COUNT(_0, _1, _2, _3, _4, _5, _6, COUNT, ...) COUNT
#define EXPAND() ,,,,,,

#define VA_SIZE(...) COMPOSE(GET_COUNT, (EXPAND __VA_ARGS__ (),0,6,5,4,3,2,1))
#define FOR_EACH(M, ...) SELECT(ARGS, VA_SIZE(__VA_ARGS__))(M, __VA_ARGS__)
#define VA_SELECT(M, ...) SELECT(M, VA_SIZE(__VA_ARGS__))(__VA_ARGS__)

#define ARGS_0(M, X)      
#define ARGS_1(M, X)      M(X)
#define ARGS_2(M, X, ...) M(X)ARGS_1(M, __VA_ARGS__)
#define ARGS_3(M, X, ...) M(X)ARGS_2(M, __VA_ARGS__)
#define ARGS_4(M, X, ...) M(X)ARGS_3(M, __VA_ARGS__)
#define ARGS_5(M, X, ...) M(X)ARGS_4(M, __VA_ARGS__)
#define ARGS_6(M, X, ...) M(X)ARGS_5(M, __VA_ARGS__)


#define TYPE_OF(value) std::remove_reference<decltype(value)>::type


#define METHOD_FN(OBJ, FUNC) std::bind(std::mem_fn(&TYPE_OF(*OBJ)::FUNC), \
                                    OBJ, std::placeholders::_1)

#define INITIALIZER_IMPL(ID) \
    static void init_f_ ## ID(void); \
    struct init_t_ ## ID { init_t_ ## ID(void) { init_f_ ## ID(); } }; \
    static init_t_ ## ID init_i_ ## ID; \
    static void init_f_ ## ID(void)


#define INITIALIZER_ID(ID) INITIALIZER_IMPL(ID)
#define INITIALIZER(ID) INITIALIZER_ID(MACRO_CONCAT(ID, __COUNTER__))


#define METHOD_TYPE(NAME) \
    decltype(std::declval<T>().NAME(), void())


#define CHECK_HAS_METHOD(NAME) \
    template<class T, typename = void> \
    struct has_ ## NAME: std::false_type {}; \
    template<class T> \
    struct has_ ## NAME<T, METHOD_TYPE(NAME)>: std::true_type {};


#define HAS_METHOD(CLS, METHOD) has_##METHOD<CLS>::value


#define DEFINE_LITERAL(FROM, TO) \
    inline constexpr TO operator "" _##TO(FROM value) noexcept { \
        return static_cast<TO>(value); \
    }


DEFINE_LITERAL(unsigned long long, int8_t)
DEFINE_LITERAL(unsigned long long, int16_t)
DEFINE_LITERAL(unsigned long long, int32_t)
DEFINE_LITERAL(unsigned long long, int64_t)
DEFINE_LITERAL(unsigned long long, uint8_t)
DEFINE_LITERAL(unsigned long long, uint16_t)
DEFINE_LITERAL(unsigned long long, uint32_t)
DEFINE_LITERAL(unsigned long long, uint64_t)


constexpr uint64_t fnv1a_32(char const* val, size_t n) {
    return ((n ? fnv1a_32(val, n - 1) : 2166136261u) ^ val[n]) * 16777619u;
}

constexpr uint64_t operator"" _hash(char const* val, size_t n) {
    return fnv1a_32(val, n);
}


class QWidget;
typedef QVector<QPair<int, QString>> EnumInfo;


EnumInfo getEnums(const QMetaObject& meta_class, const QString& id);

QString getLanguage();

template<class T>
T config(const QString& id, const T& def=T());

struct Null {};


#include "utils.T"


#endif // __UTILS_H__