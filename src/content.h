// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONTENT_H__
#define __CONTENT_H__


#include <QString>
#include <QAbstractListModel>

struct ContentDesc {
    QString title;
    QString original_title;
    QString date;
    QString language;
};

struct Content {
    QString name;
    QString provider;
    quint64 size;
    QString type;
    QVariantMap data;
};

class ContentManager: public QAbstractListModel {

    Q_OBJECT

public:

    enum ContentRoles {
        NameRole = 0,
        ProviderRole,
        SizeRole,
        TypeRole,
        DataRole
    };

    explicit ContentManager(QObject* parent = nullptr);
    virtual ~ContentManager() = default;

    virtual QVariant data(  const QModelIndex &index,
                            int role = Qt::DisplayRole) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QHash<int, QByteArray> roleNames() const override;

    void clear();
    void addContent(const Content& content);
    
    Q_INVOKABLE QVariantMap get(int row);

private:
    QVector<Content> m_contents;
};

#endif // __CONTENT_H__