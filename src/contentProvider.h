// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONTENTPROVIDER_H__
#define __CONTENTPROVIDER_H__

#include <QtCore/QObject>
#include <QtCore/QString>

#include "content.h"


class ContentProvider: public QObject {

public:
    virtual ~ContentProvider() = default;
    virtual void exitContext() = 0;
    virtual void content(const ContentDesc& desc) = 0;
};

Q_DECLARE_INTERFACE(ContentProvider, "org.openflix.content")


#endif // __CONTENTPROVIDER_H__