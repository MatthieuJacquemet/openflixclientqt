// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QQmlNetworkAccessManagerFactory>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QSslConfiguration>
#include <QCommandLineParser>
#include <QPluginLoader>
#include <QQuickView>
#include <QQuickItem>
#include <QLayout>
#include <QQmlContext>

#include "video_grid_item.h"
#include "pluginManager.h"
#include "openFlixApp.h"
#include "flowlayout.h"
#include "config.h"
#include "utils.h"
#include "contentProvider.h"
#include "contentAccess.h"
#include "metaDataProvider.h"


using namespace std;


// #ifdef Q_OS_ANDROID

// #include <android/log.h>

// void androidMessageHandler( QtMsgType type,
//                             const QMessageLogContext& context,
//                             const QString& msg)
// {
//     // QString report = msg;

//     // if (context.file && !QString(context.file).isEmpty()) {
//     //     report += " in file ";
//     //     report += QString(context.file);
//     //     report += " line ";
//     //     report += QString::number(context.line);
//     // }

//     // if (context.function && !QString(context.function).isEmpty()) {
//     //     report += " function ";
//     //     report += QString(context.function);
//     // }
//     // const char* local = report.toLocal8Bit().constData();

//     int prio = ANDROID_LOG_VERBOSE;

//     switch (type) {
//         case QtDebugMsg:
//             prio = ANDROID_LOG_DEBUG; break;
//         case QtInfoMsg:
//             prio = ANDROID_LOG_INFO; break;
//         case QtWarningMsg:
//             prio = ANDROID_LOG_WARN; break;
//         case QtCriticalMsg:
//             prio = ANDROID_LOG_ERROR; break;
//         case QtFatalMsg:
//             prio = ANDROID_LOG_FATAL; break;
//         default: break;
//     }
//     __android_log_write(prio, APP_NAME, msg.toLocal8Bit());
// }

// #endif




void OpenFlixApp::loadEngine() {

    QQmlContext* context = m_engine.rootContext();

    context->setContextProperty("app", this);

    m_engine.load("qrc:/qml/main.qml");

    if (m_engine.rootObjects().isEmpty())
        exit(EXIT_FAILURE);
}


void OpenFlixApp::setup() {

    QString plugin_path = parser.value("plugin-path");

    if (!plugin_path.isEmpty()) {
        PluginManager* mgr = PluginManager::globalInstance();
        mgr->loadPlugins(plugin_path);
    }    
}


void OpenFlixApp::appendGridItem(const ItemData& item) {
    m_items_model.addItem(item);
    Q_EMIT itemsChanged();
}

void OpenFlixApp::addContent(const Content& content) {
    m_contents_model.addContent(content);
    Q_EMIT contentsChanged();
}


MediaManager* OpenFlixApp::itemModel() {
    return &m_items_model;    
}

ContentManager* OpenFlixApp::contentModel() {
    return &m_contents_model;
}



void OpenFlixApp::fetchMoreItems() {

    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<MetaDataProvider>())
        plugin->fetchMore();
}


void OpenFlixApp::search(const QString& name) {

    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<MetaDataProvider>())
        plugin->search(name);
}


void OpenFlixApp::exitContext() {

    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<MetaDataProvider>())
        plugin->exitContext();

    for (auto& plugin: mgr->getPlugins<ContentProvider>())
        plugin->exitContext();
    
    m_contents_model.clear();
}


void OpenFlixApp::getDetails(int id) {

    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<MetaDataProvider>())
        plugin->getDetails(id);
}


void OpenFlixApp::content(  const QString& title, const QString& original_title, 
                            const QString& date, const QString& language)
{
    PluginManager* mgr = PluginManager::globalInstance();

    ContentDesc desc;
    desc.title = title;
    desc.original_title = original_title;
    desc.language = language;
    desc.date = date;

    for (auto& plugin: mgr->getPlugins<ContentProvider>())
        plugin->content(desc);
}


void OpenFlixApp::startContent(const QString& type, const QVariantMap& data) {
    
    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<ContentAccess>()) {
        if (plugin->getTypes().contains(type)) {
            plugin->startContent(data);
            break;
        }
    }
}


void OpenFlixApp::stopContent() {

    PluginManager* mgr = PluginManager::globalInstance();

    for (auto& plugin: mgr->getPlugins<ContentAccess>())
        plugin->stopContent();
}



void OpenFlixApp::parseOptions() {

    const QString APP_DESC=QCoreApplication::translate("main",
        "OpenFlix is an open streaming application, it relies on \
        plugins that bring content video content and metadata.");

    static const QCommandLineOption OPT_PLUGIN = {
        {"p","plugin-path"},
        QCoreApplication::translate("main","Path to the plugin directory."),
        "plugin_path",
        settings.value("plugin/path", relativePath("plugins")).toString()
    };

    parser.setApplicationDescription(APP_DESC);

    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(OPT_PLUGIN);

    parser.process(*this);
}


OpenFlixApp::OpenFlixApp(int argc, char *argv[]) : 
    QApplication(argc, argv),
    m_engine(this),
    m_translator(this),
    m_items_model(this),
    m_contents_model(this)
{
    // m_translator->load(locale);
    // installTranslator(m_translator);

// #ifdef Q_OS_ANDROID
//     qInstallMessageHandler(androidMessageHandler);
// #endif
    settings.setValue("client/providers", "openflix-server.herokuapp.com");
    
    parseOptions();
    setup();
    loadEngine();
}



OpenFlixApp* OpenFlixApp::globalInstance() {
    return static_cast<OpenFlixApp*>(instance());
}


QString OpenFlixApp::relativePath(const QString& name) {
    
    QDir dir(applicationDirPath());
    return dir.absoluteFilePath(name);
}



INITIALIZER(application) {

    QLocale::setDefault(QLocale::c());

	OpenFlixApp::setOrganizationName(ORG_NAME);
	OpenFlixApp::setApplicationName(APP_NAME);
    OpenFlixApp::setApplicationVersion(APP_VERSION);

	OpenFlixApp::setAttribute(Qt::AA_ShareOpenGLContexts);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QSslConfiguration conf = QSslConfiguration::defaultConfiguration();
    conf.setProtocol(QSsl::AnyProtocol);
    conf.setPeerVerifyMode(QSslSocket::VerifyNone);

    QSslConfiguration::setDefaultConfiguration(conf);

    QSettings::setDefaultFormat(QSettings::IniFormat);
}
