// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PLUGINMANAGER_H__
#define __PLUGINMANAGER_H__

#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtCore/QVector>
#include <QtCore/qglobal.h>

#include "singleton.h"
#include "openFlixApp.h"

class TerrainStagePlugin;
class PluginBase;


class PluginHandlerBase {

public:
    ~PluginHandlerBase() = default;

    virtual bool registerPlugin(QObject* plugin) = 0;

protected:
    PluginHandlerBase() = default;
};


template<class T>
struct PluginHandler: public PluginHandlerBase,
                      public QMap<QString, QSharedPointer<T>>
{
    using Container = QMap<QString, QSharedPointer<T>>;
    virtual bool registerPlugin(QObject* plugin) override;
};


class PluginManager final: public Singleton<PluginManager> {

public:
    ~PluginManager() = default;

    void loadPlugins(const QDir& path);

    template<class T> QList<QSharedPointer<T>> getPlugins() const;

    template<class T> T* getPlugin(const QString& id) const;

    template<class T> bool registerHandler();
    template<class T> bool unregisterHandler();

private:
    template<class T> PluginHandler<T>* getHandler() const;

    Q_DISABLE_COPY(PluginManager)
    PluginManager() = default;

    bool loadPlugin(const QString& name);
    bool registerPlugin(const QString& iid, QObject* plugin);

    typedef QMap<QString, QSharedPointer<PluginHandlerBase>> Handlers;
    Handlers handlers;

    friend class Singleton<PluginManager>;
};


#include "pluginManager.T"

#endif // __PLUGINMANAGER_H__