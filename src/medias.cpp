#include <QDebug>
#include "medias.h"

QVariant MediaManager::data(const QModelIndex &index, int role) const {


    if (!index.isValid() || index.row() >= m_items.size())
        return QVariant();

    QVariant value;
    ItemData item = m_items.at(index.row());

    switch(role) {
        case TitleRole:
            return item.title;
        case YearRole:
            return item.year;
        case PosterRole:
            return item.poster;
        case IdRole:
            return item.id;
    }

    return QVariant();
}

int MediaManager::rowCount(const QModelIndex &parent) const {

    if (parent.isValid())
        return 0;

    return m_items.size();
}


QHash<int, QByteArray> MediaManager::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[TitleRole]    = "title";
    roles[YearRole]     = "year";
    roles[PosterRole]   = "poster";
    roles[IdRole]       = "id";

    return roles;
}


void MediaManager::addItem(const ItemData& data) {

    int i = m_items.size();
    beginInsertRows(QModelIndex(), i, i);
    m_items.push_back(data);
    endInsertRows();
}


void MediaManager::clear() {

    beginResetModel();
    m_items.clear();
    endResetModel();
}


MediaManager::MediaManager(QObject* parent):
    QAbstractListModel(parent)
{
    
}
