#include <QDebug>
#include "content.h"

QVariant ContentManager::data(const QModelIndex &index, int role) const {

    if (!index.isValid() || index.row() >= m_contents.size())
        return QVariant();

    QVariant value;
    Content content = m_contents.at(index.row());
        
    switch(role) {
        case NameRole:
            return content.name;
        case ProviderRole:
            return content.provider;
        case SizeRole:
            return content.size;
        case TypeRole:
            return content.type;
        case DataRole:
            return content.data;
    }

    return QVariant();
}

int ContentManager::rowCount(const QModelIndex &parent) const {

    if (parent.isValid())
        return 0;

    return m_contents.size();
}


QHash<int, QByteArray> ContentManager::roleNames() const {

    QHash<int, QByteArray> roles;
    roles[NameRole]     = "name";
    roles[ProviderRole] = "provider";
    roles[SizeRole]     = "size";
    roles[TypeRole]     = "type";
    roles[DataRole]     = "data";

    return roles;
}


void ContentManager::addContent(const Content& content) {

    int i = m_contents.size();
    beginInsertRows(QModelIndex(), i, i);
    m_contents.push_back(content);
    endInsertRows();

}


void ContentManager::clear() {

    beginResetModel();
    m_contents.clear();
    endResetModel();
}


QVariantMap ContentManager::get(int row) {

    if (row < 0 || row >= m_contents.size())
        return QVariantMap();

    QVariantMap map;
    Content content = m_contents.at(row);

    map["name"]     = content.name;
    map["provider"] = content.provider;
    map["size"]     = content.size;
    map["type"]     = content.type;
    map["data"]     = content.data;

    return map;
}

ContentManager::ContentManager(QObject* parent):
    QAbstractListModel(parent)
{
    
}