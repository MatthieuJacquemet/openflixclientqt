// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONTENTACCESS_H__
#define __CONTENTACCESS_H__


#include <QtCore/QObject>
#include <QtCore/QString>


class ContentAccess: public QObject {

public:

    virtual ~ContentAccess() = default;
    virtual void startContent(QVariantMap data) = 0;
    virtual void stopContent() = 0;
    virtual void exitContext() = 0;
    virtual QVector<QString> getTypes() = 0;
};

Q_DECLARE_INTERFACE(ContentAccess, "org.openflix.access")


#endif // __CONTENTACCESS_H__