#include "openFlixApp.h"

using namespace std;

Q_DECL_EXPORT int main(int argc, char *argv[]) {   

    OpenFlixApp app(argc, argv);
    return app.exec();
}
