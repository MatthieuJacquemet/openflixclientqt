// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QtCore/QtPlugin>

#include "peerflix_controler_interface/src/peerFlixController.h"
#include "content_provider_client/src/contentClient.h"
#include "tmdb_metadata_provider/src/tmdbProvider.h"


Q_IMPORT_PLUGIN(PeerFlixController)
Q_IMPORT_PLUGIN(ContentClient)
Q_IMPORT_PLUGIN(TmdbProvider)